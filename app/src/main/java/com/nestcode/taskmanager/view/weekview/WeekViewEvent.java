package com.nestcode.taskmanager.view.weekview;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.nestcode.taskmanager.view.weekview.WeekViewUtil.isSameDay;

public class WeekViewEvent extends SugarRecord {
    private String tos;
    private Long startTime;
    private Long endTime;
    private int repeatType;
    private Long repeatEnds;
    private int color;

    public WeekViewEvent(){}

    public WeekViewEvent(String tos, Long startTime, Long endTime, int repeatType, Long repeatEnds, int color) {
        this.tos = tos;
        this.startTime = startTime;
        this.endTime = endTime;
        this.repeatType = repeatType;
        this.repeatEnds = repeatEnds;
        this.color = color;
    }

    public String getTos() {
        return tos;
    }

    public void setTos(String tos) {
        this.tos = tos;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public int getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(int repeatType) {
        this.repeatType = repeatType;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public Long getRepeatEnds() {
        return repeatEnds;
    }

    public void setRepeatEnds(Long repeatEnds) {
        this.repeatEnds = repeatEnds;
    }


    public List<WeekViewEvent> splitWeekViewEvents(){

        List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
        Calendar endTime = Calendar.getInstance();
        endTime.setTimeInMillis(this.getEndTime());
        endTime.add(Calendar.MILLISECOND, -1);

        Calendar startTime = Calendar.getInstance();
        startTime.setTimeInMillis(this.getStartTime());

        if (!isSameDay(startTime, endTime)) {
            endTime = startTime;
            endTime.set(Calendar.HOUR_OF_DAY, 23);
            endTime.set(Calendar.MINUTE, 59);
            WeekViewEvent event1 = new WeekViewEvent(this.getTos(), this.getStartTime(), this.getEndTime(), this.getRepeatType(), this.getRepeatEnds(), this.getColor());
            event1.setColor(this.getColor());
            events.add(event1);

            Calendar otherDay = startTime;
            otherDay.add(Calendar.DATE, 1);
            while (!isSameDay(otherDay, endTime)) {
                Calendar overDay = (Calendar) otherDay.clone();
                overDay.set(Calendar.HOUR_OF_DAY, 0);
                overDay.set(Calendar.MINUTE, 0);
                Calendar endOfOverDay = (Calendar) overDay.clone();
                endOfOverDay.set(Calendar.HOUR_OF_DAY, 23);
                endOfOverDay.set(Calendar.MINUTE, 59);
                WeekViewEvent eventMore = new WeekViewEvent(this.getTos(), overDay.getTimeInMillis(), endOfOverDay.getTimeInMillis(), this.getRepeatType(), this.getRepeatEnds(), this.getColor());
                eventMore.setColor(this.getColor());
                events.add(eventMore);
                otherDay.add(Calendar.DATE, 1);
            }

            Calendar time = endTime;
            time.set(Calendar.HOUR_OF_DAY, 0);
            time.set(Calendar.MINUTE, 0);
            WeekViewEvent event2 = new WeekViewEvent(this.getTos(), time.getTimeInMillis(), this.getEndTime(), this.getRepeatType(), this.getRepeatEnds(), this.getColor());
            event2.setColor(this.getColor());
            events.add(event2);
        }
        else{
            events.add(this);
        }
        return events;
    }
}
