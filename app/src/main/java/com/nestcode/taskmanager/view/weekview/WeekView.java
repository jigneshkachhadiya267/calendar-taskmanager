package com.nestcode.taskmanager.view.weekview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.OverScroller;

import com.nestcode.taskmanager.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static com.nestcode.taskmanager.activity.BaseActivity.df;
import static com.nestcode.taskmanager.activity.BaseActivity.dpToPx;
import static com.nestcode.taskmanager.activity.BaseActivity.tf;
import static com.nestcode.taskmanager.activity.BaseActivity.tfs;
import static com.nestcode.taskmanager.view.weekview.WeekView.Direction.VERTICAL;
import static com.nestcode.taskmanager.view.weekview.WeekViewUtil.daysBetween;
import static com.nestcode.taskmanager.view.weekview.WeekViewUtil.getPassedMinutesInDay;
import static com.nestcode.taskmanager.view.weekview.WeekViewUtil.isSameDay;
import static com.nestcode.taskmanager.view.weekview.WeekViewUtil.today;

public class WeekView extends View {

    private DateChangeListener mDateChangeListener;
    private Date currentDate;

    private WeekViewDragListener mWeekViewDragListener;
    private boolean longPressed = false;

    public enum Direction {
        NONE, LEFT, RIGHT, VERTICAL
    }

    protected float startOriginForScale = 0;
    protected float startOriginForScroll = 0;

    protected float distanceDone = 0;
    protected float sizeOfWeekView;
    protected float distanceMin;
    protected boolean isScaling = false;

    protected int offsetValueToSecureScreen = 9;

    @Deprecated
    public static final int LENGTH_SHORT = 1;
    @Deprecated
    public static final int LENGTH_LONG = 2;
    private final Context mContext;
    private Calendar mHomeDate;
    private Calendar mMinDate;
    private Calendar mMaxDate;
    private Paint mTimeTextPaint;
    private float mTimeTextWidth;
    private float mTimeTextHeight;
    private Paint mHeaderTextPaint;
    private Paint mHeaderTextPaintBold;
    private float mHeaderTextHeight;
    private float mHeaderHeight;
    private GestureDetectorCompat mGestureDetector;
    private OverScroller mScroller;
    private PointF mCurrentOrigin = new PointF(0f, 0f);
    private Direction mCurrentScrollDirection = Direction.NONE;
    private Paint mHeaderBackgroundPaint;
    private Paint weekviewSeperator;
    private float mWidthPerDay;
    private Paint mDayBackgroundPaint;
    private Paint mHourSeparatorPaint;
    private float mHeaderMarginBottom;
    private Paint mTodayBackgroundPaint;
    private Paint mFutureBackgroundPaint;
    private Paint mPastBackgroundPaint;
    private Paint mFutureWeekendBackgroundPaint;
    private Paint mPastWeekendBackgroundPaint;
    private Paint mNowLinePaint;
    private Paint mTodayHeaderTextPaint;
    private Paint mTodayHeaderTextPaintBold;
    private Paint mEventBackgroundPaint;
    private Paint mNewEventBackgroundPaint;
    private float mHeaderColumnWidth;
    private List<EventRect> mEventRects;
    private List<? extends WeekViewEvent> mPreviousPeriodEvents;
    private List<? extends WeekViewEvent> mCurrentPeriodEvents;
    private List<? extends WeekViewEvent> mNextPeriodEvents;
    private TextPaint mEventTextPaint;
    private TextPaint mNewEventTextPaint;
    private Paint mHeaderColumnBackgroundPaint;
    private int mFetchedPeriod = -1;
    private boolean mRefreshEvents = false;
    private Direction mCurrentFlingDirection = Direction.NONE;
    private ScaleGestureDetector mScaleDetector;
    private boolean mIsZooming;
    private Calendar mFirstVisibleDay;
    private Calendar mLastVisibleDay;
    private int mMinimumFlingVelocity = 0;
    private int mScaledTouchSlop = 0;
    private EventRect mNewEventRect;
    private int mHourHeight = 50;
    private int mNewHourHeight = -1;
    private int mMinHourHeight = 0;
    private int mEffectiveMinHourHeight = mMinHourHeight;
    private int mMaxHourHeight = 250;
    private int mColumnGap = 10;
    private int mFirstDayOfWeek = Calendar.MONDAY;
    private int mTextSize = 12;
    private int mHeaderColumnPadding = 10;
    private int mHeaderColumnTextColor = Color.DKGRAY;
    private int mNumberOfVisibleDays = 3;
    private int mHeaderRowPadding = 10;
    private int mHeaderRowBackgroundColor = Color.WHITE;
    private int mDayBackgroundColor = Color.rgb(245, 245, 245);
    private int mPastBackgroundColor = Color.rgb(227, 227, 227);
    private int mFutureBackgroundColor = Color.rgb(245, 245, 245);
    private int mPastWeekendBackgroundColor = 0;
    private int mFutureWeekendBackgroundColor = 0;
    private int mNowLineColor = Color.rgb(102, 102, 102);
    private int mNowLineThickness = 5;
    private int mHourSeparatorColor = Color.rgb(230, 230, 230);
    private int mTodayBackgroundColor = Color.rgb(239, 247, 254);
    private int mHourSeparatorHeight = 2;
    private int mTodayHeaderTextColor = Color.rgb(39, 137, 228);
    private int mEventTextSize = 12;
    private int mEventTextColor = Color.BLACK;
    private int mEventPadding = 8;
    private int mHeaderColumnBackgroundColor = Color.WHITE;
    private int mDefaultEventColor;
    private int mNewEventLengthInMinutes = 60;
    private int mNewEventTimeResolutionInMinutes = 15;
    private boolean mShowFirstDayOfWeekFirst = false;

    private CurrentDataUpdatedListener dataUpdatedListener;

    private boolean mIsFirstDraw = true;
    private boolean mAreDimensionsInvalid = true;
    @Deprecated
    private int mDayNameLength = LENGTH_LONG;
    private int mOverlappingEventGap = 0;
    private int mEventMarginVertical = 0;
    private float mXScrollingSpeed = 1f;
    private Calendar mScrollToDay = null;
    private double mScrollToHour = -1;
    private int mEventCornerRadius = 0;
    private boolean mShowDistinctWeekendColor = false;
    private boolean mShowNowLine = false;
    private boolean mShowDistinctPastFutureColor = false;
    private boolean mHorizontalFlingEnabled = true;
    private boolean mVerticalFlingEnabled = true;
    private int mAllDayEventHeight = 100;
    private float mZoomFocusPoint = 0;
    private boolean mZoomFocusPointEnabled = true;
    private int mScrollDuration = 250;
    private int mTimeColumnResolution = 60;
    private Typeface mTypeface = Typeface.DEFAULT;
    private int mMinTime = 0;
    private int mMaxTime = 24;
    private boolean mAutoLimitTime = false;
    private Rect rectTimeText;

    private EventClickListener mEventClickListener;
    private EventLongPressListener mEventLongPressListener;
    private WeekViewLoader mWeekViewLoader;
    private EmptyViewClickListener mEmptyViewClickListener;
    private EmptyViewLongPressListener mEmptyViewLongPressListener;
    private DateTimeInterpreter mDateTimeInterpreter;
    private ScrollListener mScrollListener;
    private AddEventClickListener mAddEventClickListener;
    int[] rainbow = getContext().getResources().getIntArray(R.array.rainbow);


    private final GestureDetector.SimpleOnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener() {

        @Override
        public boolean onDown(MotionEvent e) {
            if (getNumberOfVisibleDays() == 1) {
                goToNearestOrigin();
                return true;
            } else {
                startOriginForScale = mCurrentOrigin.x;
                isScaling = false;
                distanceDone = 0;
                return true;
            }
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (getNumberOfVisibleDays() == 1) {
                if (mIsZooming)
                    return true;

                switch (mCurrentScrollDirection) {
                    case NONE: {
                        if (Math.abs(distanceX) > Math.abs(distanceY)) {
                            if (distanceX > 0) {
                                mCurrentScrollDirection = Direction.LEFT;
                            } else {
                                mCurrentScrollDirection = Direction.RIGHT;
                            }
                        } else {
                            mCurrentScrollDirection = VERTICAL;
                        }
                        break;
                    }
                    case LEFT: {
                        if (Math.abs(distanceX) > Math.abs(distanceY) && (distanceX < -mScaledTouchSlop)) {
                            mCurrentScrollDirection = Direction.RIGHT;
                        }
                        break;
                    }
                    case RIGHT: {
                        if (Math.abs(distanceX) > Math.abs(distanceY) && (distanceX > mScaledTouchSlop)) {
                            mCurrentScrollDirection = Direction.LEFT;
                        }
                        break;
                    }
                }

                switch (mCurrentScrollDirection) {
                    case LEFT:
                    case RIGHT:
                        float minX = getXMinLimit();
                        float maxX = getXMaxLimit();
                        if ((mCurrentOrigin.x - (distanceX * mXScrollingSpeed)) > maxX) {
                            mCurrentOrigin.x = maxX;
                        } else if ((mCurrentOrigin.x - (distanceX * mXScrollingSpeed)) < minX) {
                            mCurrentOrigin.x = minX;
                        } else {
                            mCurrentOrigin.x -= distanceX * mXScrollingSpeed;
                        }
                        ViewCompat.postInvalidateOnAnimation(WeekView.this);
                        break;
                    case VERTICAL:
                        float minY = getYMinLimit();
                        float maxY = getYMaxLimit();
                        if ((mCurrentOrigin.y - (distanceY)) > maxY) {
                            mCurrentOrigin.y = maxY;
                        } else if ((mCurrentOrigin.y - (distanceY)) < minY) {
                            mCurrentOrigin.y = minY;
                        } else {
                            mCurrentOrigin.y -= distanceY;
                        }
                        ViewCompat.postInvalidateOnAnimation(WeekView.this);
                        break;
                }
                return true;
            } else {
                if (mIsZooming)
                    return true;

                if (e2.getPointerCount() == 2) {
                    isScaling = true;
                    return false;
                } else if (e2.getPointerCount() == 1 && isScaling) {
                    return false;
                }

                switch (mCurrentScrollDirection) {
                    case NONE: {
                        if (Math.abs(distanceX) > Math.abs(distanceY)) {
                            if (distanceX > 0) {
                                mCurrentScrollDirection = Direction.LEFT;
                            } else {
                                mCurrentScrollDirection = Direction.RIGHT;
                            }
                        } else {
                            mCurrentScrollDirection = VERTICAL;
                        }
                        break;
                    }
                    case LEFT: {
                        if (Math.abs(distanceX) > Math.abs(distanceY) && (distanceX < -mScaledTouchSlop)) {
                            mCurrentScrollDirection = Direction.RIGHT;
                        }
                        break;
                    }
                    case RIGHT: {
                        if (Math.abs(distanceX) > Math.abs(distanceY) && (distanceX > mScaledTouchSlop)) {
                            mCurrentScrollDirection = Direction.LEFT;
                        }
                        break;
                    }
                    default:
                        break;
                }

                float minX = getXMinLimit();
                float maxX = getXMaxLimit();

                switch (mCurrentScrollDirection) {
                    case LEFT:
                    case RIGHT:
                        if (e2.getX() < 0) {
                            distanceDone = e2.getX() - e1.getX();
                        } else {
                            distanceDone = e1.getX() - e2.getX();
                        }

                        if (!isScaling) {
                            if ((mCurrentOrigin.x - (distanceX * mXScrollingSpeed)) > maxX) {
                                mCurrentOrigin.x = maxX;
                            } else if ((mCurrentOrigin.x - (distanceX * mXScrollingSpeed)) < minX) {
                                mCurrentOrigin.x = minX;
                            } else {
                                mCurrentOrigin.x -= distanceX * mXScrollingSpeed;
                            }
                            ViewCompat.postInvalidateOnAnimation(WeekView.this);
                        }

                        break;
                    case VERTICAL:

                        float minY = getYMinLimit();
                        float maxY = getYMaxLimit();
                        if ((mCurrentOrigin.y - (distanceY)) > maxY) {
                            mCurrentOrigin.y = maxY;
                        } else if ((mCurrentOrigin.y - (distanceY)) < minY) {
                            mCurrentOrigin.y = minY;
                        } else {
                            mCurrentOrigin.y -= distanceY;
                        }
                        ViewCompat.postInvalidateOnAnimation(WeekView.this);
                        break;
                    default:
                        break;
                }
                return true;
            }
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (getNumberOfVisibleDays() == 1) {
                if (mIsZooming)
                    return true;

                if ((mCurrentFlingDirection == Direction.LEFT && !mHorizontalFlingEnabled) ||
                        (mCurrentFlingDirection == Direction.RIGHT && !mHorizontalFlingEnabled) ||
                        (mCurrentFlingDirection == VERTICAL && !mVerticalFlingEnabled)) {
                    return true;
                }

                mScroller.forceFinished(true);

                mCurrentFlingDirection = mCurrentScrollDirection;
                switch (mCurrentFlingDirection) {
                    case LEFT:
                    case RIGHT:
                        mScroller.fling((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, (int) (velocityX * mXScrollingSpeed), 0, (int) getXMinLimit(), (int) getXMaxLimit(), (int) getYMinLimit(), (int) getYMaxLimit());
                        break;
                    case VERTICAL:
                        mScroller.fling((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, 0, (int) velocityY, (int) getXMinLimit(), (int) getXMaxLimit(), (int) getYMinLimit(), (int) getYMaxLimit());
                        break;
                }

                ViewCompat.postInvalidateOnAnimation(WeekView.this);
                return true;
            } else {
                if (mIsZooming) {
                    return false;
                }

                if (mCurrentFlingDirection == VERTICAL && !mVerticalFlingEnabled) {
                    return true;
                }

                mScroller.forceFinished(true);

                mCurrentFlingDirection = mCurrentScrollDirection;
                if (mCurrentFlingDirection == VERTICAL) {
                    mScroller.fling((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, 0, (int) velocityY, (int) getXMinLimit(), (int) getXMaxLimit(), (int) getYMinLimit(), (int) getYMaxLimit());
                }

                ViewCompat.postInvalidateOnAnimation(WeekView.this);
                return true;
            }
        }


        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (mEventRects != null && mEventClickListener != null) {
                List<EventRect> reversedEventRects = mEventRects;
                Collections.reverse(reversedEventRects);
                for (EventRect eventRect : reversedEventRects) {
                    if (eventRect.rectF != null && e.getX() > eventRect.rectF.left && e.getX() < eventRect.rectF.right && e.getY() > eventRect.rectF.top && e.getY() < eventRect.rectF.bottom) {
                        mEventClickListener.onEventClick(eventRect.originalEvent, eventRect.rectF);
                        playSoundEffect(SoundEffectConstants.CLICK);
                        return super.onSingleTapConfirmed(e);
                    }
                }
            }
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                v.vibrate(100);
            }

            if (mEventLongPressListener != null && mEventRects != null) {
                List<EventRect> reversedEventRects = mEventRects;
                Collections.reverse(reversedEventRects);
                for (EventRect event : reversedEventRects) {
                    if (event.rectF != null && e.getX() > event.rectF.left && e.getX() < event.rectF.right && e.getY() > event.rectF.top && e.getY() < event.rectF.bottom) {
                        mEventLongPressListener.onEventLongPress(event.originalEvent, event.rectF);
                        performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                        return;
                    }
                }
            }
            longPressed = true;
            mWeekViewDragListener.onWeekViewStartsHandlingTouch();
            super.onLongPress(e);

        }
    };

    public WeekView(Context context) {
        this(context, null);
    }

    public WeekView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WeekView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        currentDate = Calendar.getInstance().getTime();

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.WeekView, 0, 0);
        try {
            mFirstDayOfWeek = a.getInteger(R.styleable.WeekView_firstDayOfWeek, mFirstDayOfWeek);
            mHourHeight = a.getDimensionPixelSize(R.styleable.WeekView_hourHeight, mHourHeight);
            mMinHourHeight = a.getDimensionPixelSize(R.styleable.WeekView_minHourHeight, mMinHourHeight);
            mEffectiveMinHourHeight = mMinHourHeight;
            mMaxHourHeight = a.getDimensionPixelSize(R.styleable.WeekView_maxHourHeight, mMaxHourHeight);
            mTextSize = a.getDimensionPixelSize(R.styleable.WeekView_textSize, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, mTextSize, context.getResources().getDisplayMetrics()));
            mHeaderColumnPadding = a.getDimensionPixelSize(R.styleable.WeekView_headerColumnPadding, mHeaderColumnPadding);
            mColumnGap = a.getDimensionPixelSize(R.styleable.WeekView_columnGap, mColumnGap);
            mHeaderColumnTextColor = a.getColor(R.styleable.WeekView_headerColumnTextColor, mHeaderColumnTextColor);
            mNumberOfVisibleDays = a.getInteger(R.styleable.WeekView_noOfVisibleDays, mNumberOfVisibleDays);
            mShowFirstDayOfWeekFirst = a.getBoolean(R.styleable.WeekView_showFirstDayOfWeekFirst, mShowFirstDayOfWeekFirst);
            mHeaderRowPadding = a.getDimensionPixelSize(R.styleable.WeekView_headerRowPadding, mHeaderRowPadding);
            mHeaderRowBackgroundColor = a.getColor(R.styleable.WeekView_headerRowBackgroundColor, mHeaderRowBackgroundColor);
            mDayBackgroundColor = a.getColor(R.styleable.WeekView_dayBackgroundColor, mDayBackgroundColor);
            mFutureBackgroundColor = a.getColor(R.styleable.WeekView_futureBackgroundColor, mFutureBackgroundColor);
            mPastBackgroundColor = a.getColor(R.styleable.WeekView_pastBackgroundColor, mPastBackgroundColor);
            mFutureWeekendBackgroundColor = a.getColor(R.styleable.WeekView_futureWeekendBackgroundColor, mFutureBackgroundColor);
            mPastWeekendBackgroundColor = a.getColor(R.styleable.WeekView_pastWeekendBackgroundColor, mPastBackgroundColor);
            mNowLineColor = a.getColor(R.styleable.WeekView_nowLineColor, mNowLineColor);
            mNowLineThickness = a.getDimensionPixelSize(R.styleable.WeekView_nowLineThickness, mNowLineThickness);
            mHourSeparatorColor = a.getColor(R.styleable.WeekView_hourSeparatorColor, mHourSeparatorColor);
            mTodayBackgroundColor = a.getColor(R.styleable.WeekView_todayBackgroundColor, mTodayBackgroundColor);
            mHourSeparatorHeight = a.getDimensionPixelSize(R.styleable.WeekView_hourSeparatorHeight, mHourSeparatorHeight);
            mTodayHeaderTextColor = a.getColor(R.styleable.WeekView_todayHeaderTextColor, mTodayHeaderTextColor);
            mEventTextSize = a.getDimensionPixelSize(R.styleable.WeekView_eventTextSize, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, mEventTextSize, context.getResources().getDisplayMetrics()));
            mEventTextColor = a.getColor(R.styleable.WeekView_eventTextColor, mEventTextColor);
            mNewEventLengthInMinutes = a.getInt(R.styleable.WeekView_newEventLengthInMinutes, mNewEventLengthInMinutes);
            mNewEventTimeResolutionInMinutes = a.getInt(R.styleable.WeekView_newEventTimeResolutionInMinutes, mNewEventTimeResolutionInMinutes);
            mEventPadding = a.getDimensionPixelSize(R.styleable.WeekView_hourSeparatorHeight, mEventPadding);
            mHeaderColumnBackgroundColor = a.getColor(R.styleable.WeekView_headerColumnBackground, mHeaderColumnBackgroundColor);
            mDayNameLength = a.getInteger(R.styleable.WeekView_dayNameLength, mDayNameLength);
            mOverlappingEventGap = a.getDimensionPixelSize(R.styleable.WeekView_overlappingEventGap, mOverlappingEventGap);
            mEventMarginVertical = a.getDimensionPixelSize(R.styleable.WeekView_eventMarginVertical, mEventMarginVertical);
            mXScrollingSpeed = a.getFloat(R.styleable.WeekView_xScrollingSpeed, mXScrollingSpeed);
            mEventCornerRadius = a.getDimensionPixelSize(R.styleable.WeekView_eventCornerRadius, mEventCornerRadius);
            mShowDistinctPastFutureColor = a.getBoolean(R.styleable.WeekView_showDistinctPastFutureColor, mShowDistinctPastFutureColor);
            mShowDistinctWeekendColor = a.getBoolean(R.styleable.WeekView_showDistinctWeekendColor, mShowDistinctWeekendColor);
            mShowNowLine = a.getBoolean(R.styleable.WeekView_showNowLine, mShowNowLine);
            mHorizontalFlingEnabled = a.getBoolean(R.styleable.WeekView_horizontalFlingEnabled, mHorizontalFlingEnabled);
            mVerticalFlingEnabled = a.getBoolean(R.styleable.WeekView_verticalFlingEnabled, mVerticalFlingEnabled);
            mAllDayEventHeight = a.getDimensionPixelSize(R.styleable.WeekView_allDayEventHeight, mAllDayEventHeight);
            mZoomFocusPoint = a.getFraction(R.styleable.WeekView_zoomFocusPoint, 1, 1, mZoomFocusPoint);
            mZoomFocusPointEnabled = a.getBoolean(R.styleable.WeekView_zoomFocusPointEnabled, mZoomFocusPointEnabled);
            mScrollDuration = a.getInt(R.styleable.WeekView_scrollDuration, mScrollDuration);
            mTimeColumnResolution = a.getInt(R.styleable.WeekView_timeColumnResolution, mTimeColumnResolution);
            mAutoLimitTime = a.getBoolean(R.styleable.WeekView_autoLimitTime, mAutoLimitTime);
            mMinTime = a.getInt(R.styleable.WeekView_minTime, mMinTime);
            mMaxTime = a.getInt(R.styleable.WeekView_maxTime, mMaxTime);
        } finally {
            a.recycle();
        }
        init();
    }

    private void init() {
        resetHomeDate();

        mGestureDetector = new GestureDetectorCompat(mContext, mGestureListener);
        mScroller = new OverScroller(mContext, new FastOutLinearInInterpolator());

        mMinimumFlingVelocity = ViewConfiguration.get(mContext).getScaledMinimumFlingVelocity();
        mScaledTouchSlop = ViewConfiguration.get(mContext).getScaledTouchSlop();

        mTimeTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTimeTextPaint.setTextAlign(Paint.Align.RIGHT);
        mTimeTextPaint.setTextSize(mTextSize);
        mTimeTextPaint.setColor(mHeaderColumnTextColor);
        rectTimeText = new Rect();
        final String exampleTime = (mTimeColumnResolution % 60 != 0) ? "00:00 PM" : "00 PM";
        mTimeTextPaint.getTextBounds(exampleTime, 0, exampleTime.length(), rectTimeText);
        mTimeTextWidth = mTimeTextPaint.measureText(exampleTime);
        mTimeTextHeight = rectTimeText.height();
        mHeaderMarginBottom = mTimeTextHeight / 2;
        initTextTimeWidth();

        mHeaderTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHeaderTextPaint.setColor(mHeaderColumnTextColor);
        mHeaderTextPaint.setTextAlign(Paint.Align.CENTER);
        mHeaderTextPaint.setTextSize(dpToPx(12));
        mHeaderTextPaint.getTextBounds(exampleTime, 0, exampleTime.length(), rectTimeText);
        mHeaderTextHeight = rectTimeText.height();
        mHeaderTextPaint.setTypeface(mTypeface);

        mHeaderTextPaintBold = new Paint();
        mHeaderTextPaintBold.set(mHeaderTextPaint);
        mHeaderTextPaintBold.setTextSize(dpToPx(16));
        mHeaderTextPaintBold.setTypeface(Typeface.DEFAULT_BOLD);

        mHeaderBackgroundPaint = new Paint();
        mHeaderBackgroundPaint.setColor(mHeaderRowBackgroundColor);

        mDayBackgroundPaint = new Paint();
        mDayBackgroundPaint.setColor(mDayBackgroundColor);
        mFutureBackgroundPaint = new Paint();
        mFutureBackgroundPaint.setColor(mFutureBackgroundColor);
        mPastBackgroundPaint = new Paint();
        mPastBackgroundPaint.setColor(mPastBackgroundColor);
        mFutureWeekendBackgroundPaint = new Paint();
        mFutureWeekendBackgroundPaint.setColor(mFutureWeekendBackgroundColor);
        mPastWeekendBackgroundPaint = new Paint();
        mPastWeekendBackgroundPaint.setColor(mPastWeekendBackgroundColor);

        mHourSeparatorPaint = new Paint();
        mHourSeparatorPaint.setStyle(Paint.Style.STROKE);
        mHourSeparatorPaint.setStrokeWidth(mHourSeparatorHeight);
        mHourSeparatorPaint.setColor(mHourSeparatorColor);

        weekviewSeperator = new Paint();
        weekviewSeperator.setStyle(Paint.Style.STROKE);
        weekviewSeperator.setStrokeWidth(dpToPx(0.5f));
        weekviewSeperator.setColor(Color.parseColor("#cfcfcf"));

        mNowLinePaint = new Paint();
        mNowLinePaint.setStrokeWidth(mNowLineThickness);
        mNowLinePaint.setColor(mNowLineColor);

        mTodayBackgroundPaint = new Paint();
        mTodayBackgroundPaint.setColor(mTodayBackgroundColor);

        mTodayHeaderTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTodayHeaderTextPaint.setTextAlign(Paint.Align.CENTER);
        mTodayHeaderTextPaint.setTextSize(dpToPx(12));
        mTodayHeaderTextPaint.setTypeface(mTypeface);
        mTodayHeaderTextPaint.setColor(mTodayHeaderTextColor);

        mTodayHeaderTextPaintBold = new Paint();
        mTodayHeaderTextPaintBold.set(mTodayHeaderTextPaint);
        mTodayHeaderTextPaintBold.setTextSize(dpToPx(17));
        mTodayHeaderTextPaintBold.setTypeface(Typeface.DEFAULT_BOLD);

        mEventBackgroundPaint = new Paint();
        mEventBackgroundPaint.setColor(Color.rgb(174, 208, 238));
        mNewEventBackgroundPaint = new Paint();
        mNewEventBackgroundPaint.setColor(Color.rgb(60, 147, 217));

        mHeaderColumnBackgroundPaint = new Paint();
        mHeaderColumnBackgroundPaint.setColor(mHeaderColumnBackgroundColor);

        mEventTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        mEventTextPaint.setStyle(Paint.Style.FILL);
        mEventTextPaint.setColor(mEventTextColor);
        mEventTextPaint.setTextSize(mEventTextSize);
        mDefaultEventColor = Color.parseColor("#9fc6e7");
        mScaleDetector = new ScaleGestureDetector(mContext, new WeekViewGestureListener());
    }

    private void resetHomeDate() {
        Calendar newHomeDate = today();

        if (getNumberOfVisibleDays() != 1) {
            newHomeDate.set(Calendar.DAY_OF_WEEK, 1);
        }

        if (mMinDate != null && newHomeDate.before(mMinDate)) {
            newHomeDate = (Calendar) mMinDate.clone();
        }
        if (mMaxDate != null && newHomeDate.after(mMaxDate)) {
            newHomeDate = (Calendar) mMaxDate.clone();
        }

        if (mMaxDate != null) {
            Calendar date = (Calendar) mMaxDate.clone();
            date.add(Calendar.DATE, 1 - getRealNumberOfVisibleDays());
            while (date.before(mMinDate)) {
                date.add(Calendar.DATE, 1);
            }

            if (newHomeDate.after(date)) {
                newHomeDate = date;
            }
        }

        mHomeDate = newHomeDate;
    }

    private float getXOriginForDate(Calendar date) {
        return -daysBetween(mHomeDate, date) * (mWidthPerDay + mColumnGap);
    }

    private int getNumberOfPeriods() {
        return (int) ((mMaxTime - mMinTime) * (60.0 / mTimeColumnResolution));
    }

    private float getYMinLimit() {
        return -(mHourHeight * (mMaxTime - mMinTime)
                + mHeaderHeight
                + mHeaderRowPadding * 2
                + mHeaderMarginBottom
                + mTimeTextHeight / 2
                - getHeight());
    }

    private float getYMaxLimit() {
        return 0;
    }

    private float getXMinLimit() {
        if (mMaxDate == null) {
            return Integer.MIN_VALUE;
        } else {
            Calendar date = (Calendar) mMaxDate.clone();
            date.add(Calendar.DATE, 1 - getRealNumberOfVisibleDays());
            while (date.before(mMinDate)) {
                date.add(Calendar.DATE, 1);
            }

            return getXOriginForDate(date);
        }
    }

    private float getXMaxLimit() {
        if (mMinDate == null) {
            return Integer.MAX_VALUE;
        } else {
            return getXOriginForDate(mMinDate);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mAreDimensionsInvalid = true;
    }

    private void initTextTimeWidth() {
        mTimeTextWidth = 0;
        for (int i = 0; i < getNumberOfPeriods(); i++) {
            String time = getDateTimeInterpreter().interpretTime(i, (i % 2) * 30);
            if (time == null)
                throw new IllegalStateException("A DateTimeInterpreter must not return null time");
            mTimeTextWidth = Math.max(mTimeTextWidth, mTimeTextPaint.measureText(time));
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawHeaderRowAndEvents(canvas);
        drawTimeColumnAndAxes(canvas);
    }

    private void calculateHeaderHeight() {
        if (getNumberOfVisibleDays() == 1) {
            mHeaderHeight = dpToPx(0);
            mHeaderTextHeight = 0;
            mHeaderMarginBottom = 0;
            mHeaderRowPadding = 0;
        } else {
            mHeaderHeight = mHeaderTextHeight + dpToPx(75);
            mHeaderTextHeight = rectTimeText.height();
            mHeaderMarginBottom = mTimeTextHeight / 2;
            mHeaderRowPadding = 10;
        }
    }

    private void drawTimeColumnAndAxes(Canvas canvas) {

        Paint p = new Paint();
        p.set(mTimeTextPaint);
        p.setColor(mTodayHeaderTextColor);

        RectF r = new RectF(0, 0, mTimeTextWidth + mHeaderColumnPadding * 2, getHeight());
        canvas.drawRect(r, mEventTextPaint);
        canvas.drawRect(0, mHeaderHeight + mHeaderRowPadding * 2, mHeaderColumnWidth, getHeight(), mHeaderColumnBackgroundPaint);
        canvas.save();
        canvas.clipRect(0, mHeaderHeight + mHeaderRowPadding * 2, mHeaderColumnWidth, getHeight());
        canvas.restore();
        for (int i = 0; i < getNumberOfPeriods(); i++) {
            float timeSpacing;
            int minutes;
            int hour;

            float timesPerHour = (float) 60.0 / mTimeColumnResolution;

            timeSpacing = mHourHeight / timesPerHour;
            hour = mMinTime + i / (int) (timesPerHour);
            minutes = i % ((int) timesPerHour) * (60 / (int) timesPerHour);

            float top = mHeaderHeight + mHeaderRowPadding * 2 + mCurrentOrigin.y + timeSpacing * i + mHeaderMarginBottom;
            String time = getDateTimeInterpreter().interpretTime(hour, minutes);
            if (time == null)
                throw new IllegalStateException("A DateTimeInterpreter must not return null time");
            if (top < getHeight()) {
                if (hour == 12 && time.equalsIgnoreCase("0:00 PM")) {
                    canvas.drawText("12:00 PM", mTimeTextWidth + mHeaderColumnPadding, top + mTimeTextHeight, mTimeTextPaint);
                } else {
                    canvas.drawText(time, mTimeTextWidth + mHeaderColumnPadding, top + mTimeTextHeight, mTimeTextPaint);
                }
            }
        }

        if (mShowNowLine) {
            float startY = mHeaderHeight + mHeaderRowPadding * 2 + mTimeTextHeight / 2 + mHeaderMarginBottom + mCurrentOrigin.y;
            Calendar now = Calendar.getInstance();
            float beforeNow = (now.get(Calendar.HOUR_OF_DAY) - mMinTime + now.get(Calendar.MINUTE) / 60.0f) * mHourHeight;
            float top = startY + beforeNow;

            String currentTime = tfs.format(now.getTime()).toUpperCase();
            canvas.drawText(currentTime, mTimeTextWidth + mHeaderColumnPadding, top, p);
        }
    }

    private void drawHeaderRowAndEvents(Canvas canvas) {
        mHeaderColumnWidth = mTimeTextWidth + mHeaderColumnPadding * 2;
        mWidthPerDay = getWidth() - mHeaderColumnWidth - mColumnGap * (getRealNumberOfVisibleDays() - 1);
        mWidthPerDay = mWidthPerDay / getRealNumberOfVisibleDays();

        calculateHeaderHeight();
        Calendar today = today();

        if (mAreDimensionsInvalid) {
            mEffectiveMinHourHeight = Math.max(mMinHourHeight, (int) ((getHeight() - mHeaderHeight - mHeaderRowPadding * 2 - mHeaderMarginBottom) / (mMaxTime - mMinTime)));

            mAreDimensionsInvalid = false;
            if (mScrollToDay != null)
                goToDate(mScrollToDay);

            mAreDimensionsInvalid = false;
            if (mScrollToHour >= 0)
                goToHour(mScrollToHour);

            mScrollToDay = null;
            mScrollToHour = -1;
            mAreDimensionsInvalid = false;
        }
        if (mIsFirstDraw) {
            mIsFirstDraw = false;

            if (getRealNumberOfVisibleDays() >= 7 && mHomeDate.get(Calendar.DAY_OF_WEEK) != mFirstDayOfWeek && mShowFirstDayOfWeekFirst) {
                int difference = (mHomeDate.get(Calendar.DAY_OF_WEEK) - mFirstDayOfWeek);
                mCurrentOrigin.x += (mWidthPerDay + mColumnGap) * difference;
            }
            setLimitTime(mMinTime, mMaxTime);
        }

        if (mNewHourHeight > 0) {
            if (mNewHourHeight < mEffectiveMinHourHeight)
                mNewHourHeight = mEffectiveMinHourHeight;
            else if (mNewHourHeight > mMaxHourHeight)
                mNewHourHeight = mMaxHourHeight;

            mHourHeight = mNewHourHeight;
            mNewHourHeight = -1;
        }

        if (mCurrentOrigin.y < getHeight() - mHourHeight * (mMaxTime - mMinTime) - mHeaderHeight - mHeaderRowPadding * 2 - mHeaderMarginBottom - mTimeTextHeight / 2)
            mCurrentOrigin.y = getHeight() - mHourHeight * (mMaxTime - mMinTime) - mHeaderHeight - mHeaderRowPadding * 2 - mHeaderMarginBottom - mTimeTextHeight / 2;

        if (mCurrentOrigin.y > 0) {
            mCurrentOrigin.y = 0;
        }

        int leftDaysWithGaps = (int) -(Math.ceil(mCurrentOrigin.x / (mWidthPerDay + mColumnGap)));
        float startFromPixel = mCurrentOrigin.x + (mWidthPerDay + mColumnGap) * leftDaysWithGaps + mHeaderColumnWidth;
        float startPixel = startFromPixel;

        Calendar day = (Calendar) today.clone();
        day.add(Calendar.HOUR_OF_DAY, 6);

        int lineCount = (int) ((getHeight() - mHeaderHeight - mHeaderRowPadding * 2 - mHeaderMarginBottom) / mHourHeight) + 1;

        lineCount = (lineCount) * (getRealNumberOfVisibleDays() + 1);
        float[] hourLines = new float[lineCount * 4];

        if (mEventRects != null) {
            for (EventRect eventRect : mEventRects) {
                eventRect.rectF = null;
            }
        }

        canvas.save();
        canvas.clipRect(mHeaderColumnWidth, mHeaderHeight + mHeaderRowPadding * 2 + mHeaderMarginBottom + mTimeTextHeight / 2, getWidth(), getHeight());
        canvas.restore();

        Calendar oldFirstVisibleDay = mFirstVisibleDay;
        mFirstVisibleDay = (Calendar) mHomeDate.clone();
        mFirstVisibleDay.add(Calendar.DATE, -(Math.round(mCurrentOrigin.x / (mWidthPerDay + mColumnGap))));
        if (!mFirstVisibleDay.equals(oldFirstVisibleDay) && mScrollListener != null) {
            mScrollListener.onFirstVisibleDayChanged(mFirstVisibleDay, oldFirstVisibleDay);
        }

        if (mAutoLimitTime) {
            List<Calendar> days = new ArrayList<>();
            for (int dayNumber = leftDaysWithGaps + 1;
                 dayNumber <= leftDaysWithGaps + getRealNumberOfVisibleDays();
                 dayNumber++) {
                day = (Calendar) mHomeDate.clone();
                day.add(Calendar.DATE, dayNumber - 1);
                days.add(day);
            }
            limitEventTime(days);
        }

        for (int dayNumber = leftDaysWithGaps + 1;
             dayNumber <= leftDaysWithGaps + getRealNumberOfVisibleDays() + 1;
             dayNumber++) {

            day = (Calendar) mHomeDate.clone();
            mLastVisibleDay = (Calendar) day.clone();
            day.add(Calendar.DATE, dayNumber - 1);
            mLastVisibleDay.add(Calendar.DATE, dayNumber - 2);
            boolean isToday = isSameDay(day, today);

            if (!dateIsValid(day)) {
                continue;
            }

            if (mEventRects == null || mRefreshEvents || (dayNumber == leftDaysWithGaps + 1 && mFetchedPeriod != (int) mWeekViewLoader.toWeekViewPeriodIndex(day) && Math.abs(mFetchedPeriod - mWeekViewLoader.toWeekViewPeriodIndex(day)) > 0.5)) {
                getMoreEvents(day);
                mRefreshEvents = false;
            }

            float start = (startPixel < mHeaderColumnWidth ? mHeaderColumnWidth : startPixel);
            if (mWidthPerDay + startPixel - start > 0) {
                if (mShowDistinctPastFutureColor) {
                    boolean isWeekend = day.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
                    Paint pastPaint = isWeekend && mShowDistinctWeekendColor ? mPastWeekendBackgroundPaint : mPastBackgroundPaint;
                    Paint futurePaint = isWeekend && mShowDistinctWeekendColor ? mFutureWeekendBackgroundPaint : mFutureBackgroundPaint;
                    float startY = mHeaderHeight + mHeaderRowPadding * 2 + mTimeTextHeight / 2 + mHeaderMarginBottom + mCurrentOrigin.y;

                    if (isToday) {
                        Calendar now = Calendar.getInstance();
                        float beforeNow = (now.get(Calendar.HOUR_OF_DAY) + now.get(Calendar.MINUTE) / 60.0f) * mHourHeight;
                        canvas.drawRect(start, startY, startPixel + mWidthPerDay, startY + beforeNow, pastPaint);
                        canvas.drawRect(start, startY + beforeNow, startPixel + mWidthPerDay, getHeight(), futurePaint);
                    } else if (day.before(today)) {
                        canvas.drawRect(start, startY, startPixel + mWidthPerDay, getHeight(), pastPaint);
                    } else {
                        canvas.drawRect(start, startY, startPixel + mWidthPerDay, getHeight(), futurePaint);
                    }
                } else {
                    canvas.drawRect(start, mHeaderHeight + mHeaderRowPadding * 2 + mTimeTextHeight / 2 + mHeaderMarginBottom, startPixel + mWidthPerDay, getHeight(), isToday ? mTodayBackgroundPaint : mDayBackgroundPaint);
                }
            }

            int i = 0;
            for (int hourNumber = mMinTime; hourNumber < mMaxTime; hourNumber++) {
                float top = mHeaderHeight + mHeaderRowPadding * 2 + mCurrentOrigin.y + mHourHeight * (hourNumber - mMinTime) + mTimeTextHeight / 2 + mHeaderMarginBottom;
                if (top > mHeaderHeight + mHeaderRowPadding * 2 + mTimeTextHeight / 2 + mHeaderMarginBottom - mHourSeparatorHeight && top < getHeight() && startPixel + mWidthPerDay - start > 0) {
                    hourLines[i * 4] = start;
                    hourLines[i * 4 + 1] = top;
                    hourLines[i * 4 + 2] = startPixel + mWidthPerDay;
                    hourLines[i * 4 + 3] = top;
                    i++;
                }
            }
            canvas.drawLines(hourLines, mHourSeparatorPaint);

            drawEvents(day, startPixel, canvas);

            if (mShowNowLine && isToday) {
                float startY = mHeaderHeight + mHeaderRowPadding * 2 + mTimeTextHeight / 2 + mHeaderMarginBottom + mCurrentOrigin.y;
                Calendar now = Calendar.getInstance();
                float beforeNow = (now.get(Calendar.HOUR_OF_DAY) - mMinTime + now.get(Calendar.MINUTE) / 60.0f) * mHourHeight;
                float top = startY + beforeNow;
                canvas.drawLine(start, top, startPixel + mWidthPerDay, top, mNowLinePaint);
            }
            startPixel += mWidthPerDay + mColumnGap;
        }
        canvas.save();
        canvas.clipRect(0, 0, mTimeTextWidth + mHeaderColumnPadding * 2, mHeaderHeight + mHeaderRowPadding * 2);
        canvas.restore();

        canvas.drawRect(0, 0, mTimeTextWidth + mHeaderColumnPadding * 2, mHeaderHeight + mHeaderRowPadding * 2, mHeaderBackgroundPaint);

        canvas.save();
        canvas.clipRect(mHeaderColumnWidth, 0, getWidth(), mHeaderHeight + mHeaderRowPadding * 2);
        canvas.restore();

        canvas.drawRect(0, 0, getWidth(), mHeaderHeight + mHeaderRowPadding * 2, mHeaderBackgroundPaint);

        startPixel = startFromPixel;
        for (int dayNumber = leftDaysWithGaps + 1; dayNumber <= leftDaysWithGaps + getRealNumberOfVisibleDays() + 1; dayNumber++) {
            day = (Calendar) mHomeDate.clone();
            day.add(Calendar.DATE, dayNumber - 1);
            boolean isToday = isSameDay(day, today);

            if (!dateIsValid(day))
                continue;

            String dayLabel = getDateTimeInterpreter().interpretDate(day);
            if (dayLabel == null)
                throw new IllegalStateException("A DateTimeInterpreter must not return null date");
            canvas.drawText(dayLabel.split(" ")[0], startPixel + mWidthPerDay / 2, mHeaderTextHeight + mHeaderRowPadding + dpToPx(10), mHeaderTextPaint);
            canvas.drawLine(0, dpToPx(40), getWidth(), dpToPx(40), weekviewSeperator);
            canvas.drawText(dayLabel.split(" ")[1], startPixel + mWidthPerDay / 2, mHeaderTextHeight + mHeaderRowPadding + dpToPx(58), mHeaderTextPaintBold);

            if (isToday) {
                canvas.drawRect(startPixel + mWidthPerDay / 2 - 10, mHeaderTextHeight + mHeaderRowPadding + dpToPx(74), startPixel + mWidthPerDay / 2 + dpToPx(5), mHeaderTextHeight + mHeaderRowPadding + dpToPx(76), mTodayHeaderTextPaintBold);
            }

            startPixel += mWidthPerDay + mColumnGap;
        }
    }

    private Calendar getTimeFromPoint(float x, float y) {
        int leftDaysWithGaps = (int) -(Math.ceil(mCurrentOrigin.x / (mWidthPerDay + mColumnGap)));
        float startPixel = mCurrentOrigin.x + (mWidthPerDay + mColumnGap) * leftDaysWithGaps + mHeaderColumnWidth;
        for (int dayNumber = leftDaysWithGaps + 1;
             dayNumber <= leftDaysWithGaps + getRealNumberOfVisibleDays() + 1;
             dayNumber++) {
            float start = (startPixel < mHeaderColumnWidth ? mHeaderColumnWidth : startPixel);
            if (mWidthPerDay + startPixel - start > 0 && x > start && x < startPixel + mWidthPerDay) {
                Calendar day = (Calendar) mHomeDate.clone();
                day.add(Calendar.DATE, dayNumber - 1);
                float pixelsFromZero = y - mCurrentOrigin.y - mHeaderHeight - mHeaderRowPadding * 2 - mTimeTextHeight / 2 - mHeaderMarginBottom;
                int hour = (int) (pixelsFromZero / mHourHeight);
                int minute = (int) (60 * (pixelsFromZero - hour * mHourHeight) / mHourHeight);
                day.add(Calendar.HOUR_OF_DAY, hour + mMinTime);
                day.set(Calendar.MINUTE, minute);
                return day;
            }
            startPixel += mWidthPerDay + mColumnGap;
        }
        return null;
    }

    private void limitEventTime(List<Calendar> dates) {
        if (mEventRects != null && mEventRects.size() > 0) {
            Calendar startTime = null;
            Calendar endTime = null;

            for (EventRect eventRect : mEventRects) {
                for (Calendar date : dates) {

                    Calendar ss = Calendar.getInstance();
                    Calendar ee = Calendar.getInstance();

                    ss.setTimeInMillis(eventRect.event.getStartTime());
                    ee.setTimeInMillis(eventRect.event.getEndTime());

                    if (isSameDay(ss, date)) {

                        if (startTime == null || getPassedMinutesInDay(startTime) > getPassedMinutesInDay(ss)) {
                            startTime = ss;
                        }

                        if (endTime == null || getPassedMinutesInDay(endTime) < getPassedMinutesInDay(ee)) {
                            endTime = ee;
                        }
                    }
                }
            }

            if (startTime != null && endTime != null && startTime.before(endTime)) {
                setLimitTime(Math.max(0, startTime.get(Calendar.HOUR_OF_DAY)),
                        Math.min(24, endTime.get(Calendar.HOUR_OF_DAY) + 1));
                return;
            }
        }
    }

    private void drawEvents(Calendar date, float startFromPixel, Canvas canvas) {
        if (mEventRects != null && mEventRects.size() > 0) {
            for (int i = 0; i < mEventRects.size(); i++) {

                Calendar ss = Calendar.getInstance();
                Calendar ee = Calendar.getInstance();

                ss.setTimeInMillis(mEventRects.get(i).event.getStartTime());
                ee.setTimeInMillis(mEventRects.get(i).event.getEndTime());

                if (isSameDay(ss, date)) {

                    int marginTop = mHourHeight * mMinTime;
                    float top = mHourHeight * mEventRects.get(i).top / 60 + mCurrentOrigin.y + mHeaderHeight + mHeaderRowPadding * 2 + mHeaderMarginBottom + mTimeTextHeight / 2 + mEventMarginVertical - marginTop;

                    float bottom = mEventRects.get(i).bottom;
                    bottom = mHourHeight * bottom / 60 + mCurrentOrigin.y + mHeaderHeight + mHeaderRowPadding * 2 + mHeaderMarginBottom + mTimeTextHeight / 2 - mEventMarginVertical - marginTop;

                    float left = startFromPixel + mEventRects.get(i).left * mWidthPerDay;
                    if (left < startFromPixel)
                        left += mOverlappingEventGap;
                    float right = left + mEventRects.get(i).width * mWidthPerDay;
                    if (right < startFromPixel + mWidthPerDay)
                        right -= mOverlappingEventGap;

                    if (left < right && left < getWidth() && top < getHeight() && right > mHeaderColumnWidth && bottom > mHeaderHeight + mHeaderRowPadding * 2 + mTimeTextHeight / 2 + mHeaderMarginBottom) {
                        mEventRects.get(i).rectF = new RectF(left, top, right, bottom);

                        if (mEventRects.get(i).event.getColor() < 0) {
                            mEventBackgroundPaint.setColor(rainbow[0]);
                        } else {
                            mEventBackgroundPaint.setColor(rainbow[mEventRects.get(i).event.getColor() % rainbow.length]);
                        }

                        canvas.drawRoundRect(mEventRects.get(i).rectF, mEventCornerRadius, mEventCornerRadius, mEventBackgroundPaint);
                        float topToUse = top;
                        if (ss.get(Calendar.HOUR_OF_DAY) < mMinTime)
                            topToUse = mHourHeight * getPassedMinutesInDay(mMinTime, 0) / 60 + mCurrentOrigin.y + mHeaderHeight + mHeaderRowPadding * 2 + mHeaderMarginBottom + mTimeTextHeight / 2 + mEventMarginVertical - marginTop;

                        drawEventTitle(mEventRects.get(i).event, mEventRects.get(i).rectF, canvas, topToUse, left);
                    } else
                        mEventRects.get(i).rectF = null;
                }
            }
        }
    }

    public WeekViewDragListener getmWeekViewDragListener() {
        return mWeekViewDragListener;
    }

    public void setmWeekViewDragListener(WeekViewDragListener mWeekViewDragListener) {
        this.mWeekViewDragListener = mWeekViewDragListener;
    }

    public void setDataUpdatedListener(CurrentDataUpdatedListener dataUpdatedListener) {
        this.dataUpdatedListener = dataUpdatedListener;
    }

    private void drawEventTitle(WeekViewEvent event, RectF rect, Canvas canvas, float originalTop, float originalLeft) {
        if (rect.right - rect.left - mEventPadding * 2 < 0) return;
        if (rect.bottom - rect.top - mEventPadding * 2 < 0) return;

        SpannableStringBuilder bob = new SpannableStringBuilder();
        if (event.getTos() != null) {

            Calendar ss = Calendar.getInstance();
            Calendar ee = Calendar.getInstance();

            ss.setTimeInMillis(event.getStartTime());
            ee.setTimeInMillis(event.getEndTime());

            bob.append(event.getTos()).append(" ").append(tf.format(ss.getTime())).append(" - ").append(tf.format(ee.getTime()));
            bob.setSpan(new StyleSpan(Typeface.BOLD), 0, bob.length(), 0);
        }

        int availableHeight = (int) (rect.bottom - originalTop - mEventPadding * 2);
        int availableWidth = (int) (rect.right - originalLeft - mEventPadding * 2);

        mEventTextPaint.setTextSize(dpToPx(10));
        StaticLayout textLayout = new StaticLayout(bob, mEventTextPaint, availableWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        if (textLayout.getLineCount() > 0) {
            int lineHeight = textLayout.getHeight() / textLayout.getLineCount();

            if (availableHeight >= lineHeight) {
                int availableLineCount = availableHeight / lineHeight;
                do {
                    textLayout = new StaticLayout(TextUtils.ellipsize(bob, mEventTextPaint, availableLineCount * availableWidth, TextUtils.TruncateAt.END), mEventTextPaint, (int) (rect.right - originalLeft - mEventPadding * 2), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                    availableLineCount--;

                } while (textLayout.getHeight() > availableHeight);

                canvas.save();
                canvas.translate(originalLeft + mEventPadding, originalTop + mEventPadding);
                textLayout.draw(canvas);
                canvas.restore();
            }
        }
    }

    private class EventRect {
        public WeekViewEvent event;
        public WeekViewEvent originalEvent;
        public RectF rectF;
        public float left;
        public float width;
        public float top;
        public float bottom;

        public EventRect(WeekViewEvent event, WeekViewEvent originalEvent, RectF rectF) {
            this.event = event;
            this.rectF = rectF;
            this.originalEvent = originalEvent;
        }
    }

    private void getMoreEvents(Calendar day) {

        if (mEventRects == null)
            mEventRects = new ArrayList<EventRect>();
        if (mWeekViewLoader == null && !isInEditMode())
            throw new IllegalStateException("You must provide a MonthChangeListener");


        if (mRefreshEvents) {
            mEventRects.clear();
            mPreviousPeriodEvents = new ArrayList<>();
            mCurrentPeriodEvents = new ArrayList<>();
            mNextPeriodEvents = new ArrayList<>();
            mFetchedPeriod = -1;
        }

        if (mWeekViewLoader != null) {
            int periodToFetch = (int) mWeekViewLoader.toWeekViewPeriodIndex(day);
            if (!isInEditMode() && (mFetchedPeriod < 0 || mFetchedPeriod != periodToFetch || mRefreshEvents)) {
                List<? extends WeekViewEvent> previousPeriodEvents = null;
                List<? extends WeekViewEvent> currentPeriodEvents = null;
                List<? extends WeekViewEvent> nextPeriodEvents = null;

                if (mPreviousPeriodEvents != null && mCurrentPeriodEvents != null && mNextPeriodEvents != null) {
                    if (periodToFetch == mFetchedPeriod - 1) {
                        currentPeriodEvents = mPreviousPeriodEvents;
                        nextPeriodEvents = mCurrentPeriodEvents;
                    } else if (periodToFetch == mFetchedPeriod) {
                        previousPeriodEvents = mPreviousPeriodEvents;
                        currentPeriodEvents = mCurrentPeriodEvents;
                        nextPeriodEvents = mNextPeriodEvents;
                    } else if (periodToFetch == mFetchedPeriod + 1) {
                        previousPeriodEvents = mCurrentPeriodEvents;
                        currentPeriodEvents = mNextPeriodEvents;
                    }
                }

                if (currentPeriodEvents == null)
                    currentPeriodEvents = mWeekViewLoader.onLoad(periodToFetch);
                if (previousPeriodEvents == null)
                    previousPeriodEvents = mWeekViewLoader.onLoad(periodToFetch - 1);
                if (nextPeriodEvents == null)
                    nextPeriodEvents = mWeekViewLoader.onLoad(periodToFetch + 1);

                mEventRects.clear();
                sortAndCacheEvents(previousPeriodEvents);
                sortAndCacheEvents(currentPeriodEvents);
                sortAndCacheEvents(nextPeriodEvents);
                calculateHeaderHeight();

                mPreviousPeriodEvents = previousPeriodEvents;
                mCurrentPeriodEvents = currentPeriodEvents;
                mNextPeriodEvents = nextPeriodEvents;
                mFetchedPeriod = periodToFetch;
                dataUpdatedListener.onCurrentDataUpdated(mCurrentPeriodEvents);
            }
        }

        List<EventRect> tempEvents = mEventRects;
        mEventRects = new ArrayList<EventRect>();

        while (tempEvents.size() > 0) {
            ArrayList<EventRect> eventRects = new ArrayList<>(tempEvents.size());
            EventRect eventRect1 = tempEvents.remove(0);
            eventRects.add(eventRect1);

            int i = 0;
            while (i < tempEvents.size()) {
                EventRect eventRect2 = tempEvents.get(i);
                Calendar startTime = Calendar.getInstance();
                Calendar endTime = Calendar.getInstance();
                startTime.setTimeInMillis(eventRect1.event.getStartTime());
                endTime.setTimeInMillis(eventRect2.event.getEndTime());

                if (isSameDay(startTime, endTime)) {
                    tempEvents.remove(i);
                    eventRects.add(eventRect2);
                } else {
                    i++;
                }
            }
            computePositionOfEvents(eventRects);
        }
    }

    private void cacheEvent(WeekViewEvent event) {
        if (event.getStartTime().compareTo(event.getEndTime()) >= 0)
            return;
        List<WeekViewEvent> splitedEvents = event.splitWeekViewEvents();
        for (WeekViewEvent splitedEvent : splitedEvents) {
            mEventRects.add(new EventRect(splitedEvent, event, null));
        }
    }

    private void sortAndCacheEvents(List<? extends WeekViewEvent> events) {
        sortEvents(events);
        for (WeekViewEvent event : events) {
            cacheEvent(event);
        }
    }

    public interface CurrentDataUpdatedListener {
        public void onCurrentDataUpdated(List<? extends WeekViewEvent> mCurrentPeriodEvents);
    }

    private void sortEvents(List<? extends WeekViewEvent> events) {
        Collections.sort(events, new Comparator<WeekViewEvent>() {
            @Override
            public int compare(WeekViewEvent event1, WeekViewEvent event2) {
                long start1 = event1.getStartTime();
                long start2 = event2.getStartTime();
                int comparator = start1 > start2 ? 1 : (start1 < start2 ? -1 : 0);
                if (comparator == 0) {
                    long end1 = event1.getEndTime();
                    long end2 = event2.getEndTime();
                    comparator = end1 > end2 ? 1 : (end1 < end2 ? -1 : 0);
                }
                return comparator;
            }
        });
    }

    private void computePositionOfEvents(List<EventRect> eventRects) {
        List<List<EventRect>> collisionGroups = new ArrayList<List<EventRect>>();
        for (EventRect eventRect : eventRects) {
            boolean isPlaced = false;

            outerLoop:
            for (List<EventRect> collisionGroup : collisionGroups) {
                for (EventRect groupEvent : collisionGroup) {
                    if (isEventsCollide(groupEvent.event, eventRect.event)) {
                        collisionGroup.add(eventRect);
                        isPlaced = true;
                        break outerLoop;
                    }
                }
            }

            if (!isPlaced) {
                List<EventRect> newGroup = new ArrayList<EventRect>();
                newGroup.add(eventRect);
                collisionGroups.add(newGroup);
            }
        }

        for (List<EventRect> collisionGroup : collisionGroups) {
            expandEventsToMaxWidth(collisionGroup);
        }
    }

    private void expandEventsToMaxWidth(List<EventRect> collisionGroup) {
        List<List<EventRect>> columns = new ArrayList<List<EventRect>>();
        columns.add(new ArrayList<EventRect>());
        for (EventRect eventRect : collisionGroup) {
            boolean isPlaced = false;
            for (List<EventRect> column : columns) {
                if (column.size() == 0) {
                    column.add(eventRect);
                    isPlaced = true;
                } else if (!isEventsCollide(eventRect.event, column.get(column.size() - 1).event)) {
                    column.add(eventRect);
                    isPlaced = true;
                    break;
                }
            }
            if (!isPlaced) {
                List<EventRect> newColumn = new ArrayList<EventRect>();
                newColumn.add(eventRect);
                columns.add(newColumn);
            }
        }

        int maxRowCount = 0;
        for (List<EventRect> column : columns) {
            maxRowCount = Math.max(maxRowCount, column.size());
        }
        for (int i = 0; i < maxRowCount; i++) {
            float j = 0;
            for (List<EventRect> column : columns) {
                if (column.size() >= i + 1) {

                    Calendar startTime = Calendar.getInstance();
                    Calendar endTime = Calendar.getInstance();

                    EventRect eventRect = column.get(i);
                    eventRect.width = 1f / columns.size();
                    eventRect.left = j / columns.size();

                    startTime.setTimeInMillis(eventRect.event.getStartTime());
                    endTime.setTimeInMillis(eventRect.event.getEndTime());

                    eventRect.top = getPassedMinutesInDay(startTime);
                    if (endTime.get(Calendar.HOUR_OF_DAY) == 0 && endTime.get(Calendar.MINUTE) == 0) {
                        endTime.set(Calendar.HOUR_OF_DAY, 23);
                        endTime.set(Calendar.MINUTE, 59);
                    }
                    eventRect.bottom = getPassedMinutesInDay(endTime);
                    mEventRects.add(eventRect);
                }
                j++;
            }
        }
    }

    private boolean isEventsCollide(WeekViewEvent event1, WeekViewEvent event2) {
        long start1 = event1.getStartTime();
        long end1 = event1.getEndTime();
        long start2 = event2.getStartTime();
        long end2 = event2.getEndTime();
        return !((start1 >= end2) || (end1 <= start2));
    }


    private boolean isTimeAfterOrEquals(Calendar time1, Calendar time2) {
        return !(time1 == null || time2 == null) && time1.getTimeInMillis() >= time2.getTimeInMillis();
    }

    @Override
    public void invalidate() {
        super.invalidate();
        mAreDimensionsInvalid = true;
    }

    public void setOnEventClickListener(EventClickListener listener) {
        this.mEventClickListener = listener;
    }

    public EventClickListener getEventClickListener() {
        return mEventClickListener;
    }

    public @Nullable
    MonthLoader.MonthChangeListener getMonthChangeListener() {
        if (mWeekViewLoader instanceof MonthLoader)
            return ((MonthLoader) mWeekViewLoader).getOnMonthChangeListener();
        return null;
    }

    public void setMonthChangeListener(MonthLoader.MonthChangeListener monthChangeListener) {
        this.mWeekViewLoader = new MonthLoader(monthChangeListener);
    }

    public WeekViewLoader getWeekViewLoader() {
        return mWeekViewLoader;
    }

    public void setWeekViewLoader(WeekViewLoader loader) {
        this.mWeekViewLoader = loader;
    }

    public EventLongPressListener getEventLongPressListener() {
        return mEventLongPressListener;
    }

    public void setEventLongPressListener(EventLongPressListener eventLongPressListener) {
        this.mEventLongPressListener = eventLongPressListener;
    }

    public void setEmptyViewClickListener(EmptyViewClickListener emptyViewClickListener) {
        this.mEmptyViewClickListener = emptyViewClickListener;
    }

    public EmptyViewClickListener getEmptyViewClickListener() {
        return mEmptyViewClickListener;
    }

    public void setEmptyViewLongPressListener(EmptyViewLongPressListener emptyViewLongPressListener) {
        this.mEmptyViewLongPressListener = emptyViewLongPressListener;
    }

    public EmptyViewLongPressListener getEmptyViewLongPressListener() {
        return mEmptyViewLongPressListener;
    }

    public void setScrollListener(ScrollListener scrolledListener) {
        this.mScrollListener = scrolledListener;
    }

    public ScrollListener getScrollListener() {
        return mScrollListener;
    }

    public void setTimeColumnResolution(int resolution) {
        mTimeColumnResolution = resolution;
    }

    public int getTimeColumnResolution() {
        return mTimeColumnResolution;
    }

    public void setAddEventClickListener(AddEventClickListener addEventClickListener) {
        this.mAddEventClickListener = addEventClickListener;
    }

    public AddEventClickListener getAddEventClickListener() {
        return mAddEventClickListener;
    }

    public DateTimeInterpreter getDateTimeInterpreter() {
        if (mDateTimeInterpreter == null) {
            mDateTimeInterpreter = new DateTimeInterpreter() {
                @Override
                public String interpretDate(Calendar date) {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE", Locale.getDefault());
                        return sdf.format(date.getTime()).toUpperCase();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "";
                    }
                }

                @Override
                public String interpretTime(int hour, int minutes) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY, hour);
                    calendar.set(Calendar.MINUTE, minutes);

                    try {
                        SimpleDateFormat sdf;
                        if (DateFormat.is24HourFormat(getContext())) {
                            sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
                        } else {
                            if ((mTimeColumnResolution % 60 != 0)) {
                                sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
                            } else {
                                sdf = new SimpleDateFormat("hh a", Locale.getDefault());
                            }
                        }
                        return sdf.format(calendar.getTime());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "";
                    }
                }
            };
        }
        return mDateTimeInterpreter;
    }

    public void setDateTimeInterpreter(DateTimeInterpreter dateTimeInterpreter) {
        this.mDateTimeInterpreter = dateTimeInterpreter;
        initTextTimeWidth();
    }

    public int getRealNumberOfVisibleDays() {
        if (mMinDate == null || mMaxDate == null)
            return getNumberOfVisibleDays();

        return Math.min(mNumberOfVisibleDays, daysBetween(mMinDate, mMaxDate) + 1);
    }

    public int getNumberOfVisibleDays() {
        return mNumberOfVisibleDays;
    }

    public void setNumberOfVisibleDays(int numberOfVisibleDays) {
        this.mNumberOfVisibleDays = numberOfVisibleDays;
        resetHomeDate();
        mCurrentOrigin.x = 0;
        mCurrentOrigin.y = 0;
        invalidate();
    }

    public int getHourHeight() {
        return mHourHeight;
    }

    public void setHourHeight(int hourHeight) {
        mNewHourHeight = hourHeight;
        invalidate();
    }

    public int getColumnGap() {
        return mColumnGap;
    }

    public void setColumnGap(int columnGap) {
        mColumnGap = columnGap;
        invalidate();
    }

    public int getFirstDayOfWeek() {
        return mFirstDayOfWeek;
    }

    public void setFirstDayOfWeek(int firstDayOfWeek) {
        mFirstDayOfWeek = firstDayOfWeek;
        invalidate();
    }

    public boolean isShowFirstDayOfWeekFirst() {
        return mShowFirstDayOfWeekFirst;
    }

    public void setShowFirstDayOfWeekFirst(boolean show) {
        mShowFirstDayOfWeekFirst = show;
    }

    public int getTextSize() {
        return mTextSize;
    }

    public void setTextSize(int textSize) {
        mTextSize = textSize;
        mTodayHeaderTextPaint.setTextSize(mTextSize);
        mHeaderTextPaint.setTextSize(mTextSize);
        mTimeTextPaint.setTextSize(mTextSize);
        invalidate();
    }

    public int getHeaderColumnPadding() {
        return mHeaderColumnPadding;
    }

    public void setHeaderColumnPadding(int headerColumnPadding) {
        mHeaderColumnPadding = headerColumnPadding;
        invalidate();
    }

    public int getHeaderColumnTextColor() {
        return mHeaderColumnTextColor;
    }

    public void setHeaderColumnTextColor(int headerColumnTextColor) {
        mHeaderColumnTextColor = headerColumnTextColor;
        mHeaderTextPaint.setColor(mHeaderColumnTextColor);
        mTimeTextPaint.setColor(mHeaderColumnTextColor);
        invalidate();
    }

    public void setTypeface(Typeface typeface) {
        if (typeface != null) {
            mEventTextPaint.setTypeface(typeface);
            mTodayHeaderTextPaint.setTypeface(typeface);
            mTimeTextPaint.setTypeface(typeface);
            mTypeface = typeface;
            init();
        }
    }

    public int getHeaderRowPadding() {
        return mHeaderRowPadding;
    }

    public void setHeaderRowPadding(int headerRowPadding) {
        mHeaderRowPadding = headerRowPadding;
        invalidate();
    }

    public int getHeaderRowBackgroundColor() {
        return mHeaderRowBackgroundColor;
    }

    public void setHeaderRowBackgroundColor(int headerRowBackgroundColor) {
        mHeaderRowBackgroundColor = headerRowBackgroundColor;
        mHeaderBackgroundPaint.setColor(mHeaderRowBackgroundColor);
        invalidate();
    }

    public int getDayBackgroundColor() {
        return mDayBackgroundColor;
    }

    public void setDayBackgroundColor(int dayBackgroundColor) {
        mDayBackgroundColor = dayBackgroundColor;
        mDayBackgroundPaint.setColor(mDayBackgroundColor);
        invalidate();
    }

    public int getHourSeparatorColor() {
        return mHourSeparatorColor;
    }

    public void setHourSeparatorColor(int hourSeparatorColor) {
        mHourSeparatorColor = hourSeparatorColor;
        mHourSeparatorPaint.setColor(mHourSeparatorColor);
        invalidate();
    }

    public int getTodayBackgroundColor() {
        return mTodayBackgroundColor;
    }

    public void setTodayBackgroundColor(int todayBackgroundColor) {
        mTodayBackgroundColor = todayBackgroundColor;
        mTodayBackgroundPaint.setColor(mTodayBackgroundColor);
        invalidate();
    }

    public int getHourSeparatorHeight() {
        return mHourSeparatorHeight;
    }

    public void setHourSeparatorHeight(int hourSeparatorHeight) {
        mHourSeparatorHeight = hourSeparatorHeight;
        mHourSeparatorPaint.setStrokeWidth(mHourSeparatorHeight);
        invalidate();
    }

    public int getTodayHeaderTextColor() {
        return mTodayHeaderTextColor;
    }

    public void setTodayHeaderTextColor(int todayHeaderTextColor) {
        mTodayHeaderTextColor = todayHeaderTextColor;
        mTodayHeaderTextPaint.setColor(mTodayHeaderTextColor);
        invalidate();
    }

    public int getEventTextSize() {
        return mEventTextSize;
    }

    public void setEventTextSize(int eventTextSize) {
        mEventTextSize = eventTextSize;
        mEventTextPaint.setTextSize(mEventTextSize);
        invalidate();
    }

    public int getEventTextColor() {
        return mEventTextColor;
    }

    public void setEventTextColor(int eventTextColor) {
        mEventTextColor = eventTextColor;
        mEventTextPaint.setColor(mEventTextColor);
        invalidate();
    }

    public int getEventPadding() {
        return mEventPadding;
    }

    public void setEventPadding(int eventPadding) {
        mEventPadding = eventPadding;
        invalidate();
    }

    public int getHeaderColumnBackgroundColor() {
        return mHeaderColumnBackgroundColor;
    }

    public void setHeaderColumnBackgroundColor(int headerColumnBackgroundColor) {
        mHeaderColumnBackgroundColor = headerColumnBackgroundColor;
        mHeaderColumnBackgroundPaint.setColor(mHeaderColumnBackgroundColor);
        invalidate();
    }

    public int getDefaultEventColor() {
        return mDefaultEventColor;
    }

    public void setDefaultEventColor(int defaultEventColor) {
        mDefaultEventColor = defaultEventColor;
        invalidate();
    }

    public int getNewEventLengthInMinutes() {
        return mNewEventLengthInMinutes;
    }

    public void setNewEventLengthInMinutes(int newEventLengthInMinutes) {
        this.mNewEventLengthInMinutes = newEventLengthInMinutes;
    }

    public int getNewEventTimeResolutionInMinutes() {
        return mNewEventTimeResolutionInMinutes;
    }

    public void setNewEventTimeResolutionInMinutes(int newEventTimeResolutionInMinutes) {
        this.mNewEventTimeResolutionInMinutes = newEventTimeResolutionInMinutes;
    }

    @Deprecated
    public int getDayNameLength() {
        return mDayNameLength;
    }


    @Deprecated
    public void setDayNameLength(int length) {
        if (length != LENGTH_LONG && length != LENGTH_SHORT) {
            throw new IllegalArgumentException("length parameter must be either LENGTH_LONG or LENGTH_SHORT");
        }
        this.mDayNameLength = length;
    }

    public int getOverlappingEventGap() {
        return mOverlappingEventGap;
    }

    public void setOverlappingEventGap(int overlappingEventGap) {
        this.mOverlappingEventGap = overlappingEventGap;
        invalidate();
    }

    public int getEventCornerRadius() {
        return mEventCornerRadius;
    }


    public void setEventCornerRadius(int eventCornerRadius) {
        mEventCornerRadius = eventCornerRadius;
    }

    public int getEventMarginVertical() {
        return mEventMarginVertical;
    }


    public void setEventMarginVertical(int eventMarginVertical) {
        this.mEventMarginVertical = eventMarginVertical;
        invalidate();
    }

    public Calendar getFirstVisibleDay() {
        return mFirstVisibleDay;
    }


    public Calendar getLastVisibleDay() {
        return mLastVisibleDay;
    }


    public float getXScrollingSpeed() {
        return mXScrollingSpeed;
    }


    public void setXScrollingSpeed(float xScrollingSpeed) {
        this.mXScrollingSpeed = xScrollingSpeed;
    }

    public Calendar getMinDate() {
        return mMinDate;
    }

    public void setMinDate(Calendar minDate) {
        if (minDate != null) {
            minDate.set(Calendar.HOUR_OF_DAY, 0);
            minDate.set(Calendar.MINUTE, 0);
            minDate.set(Calendar.SECOND, 0);
            minDate.set(Calendar.MILLISECOND, 0);
            if (mMaxDate != null && minDate.after(mMaxDate)) {
                throw new IllegalArgumentException("minDate cannot be later than maxDate");
            }
        }

        mMinDate = minDate;
        resetHomeDate();
        mCurrentOrigin.x = 0;
        invalidate();
    }

    public Calendar getMaxDate() {
        return mMaxDate;
    }

    public void setMaxDate(Calendar maxDate) {
        if (maxDate != null) {
            maxDate.set(Calendar.HOUR_OF_DAY, 0);
            maxDate.set(Calendar.MINUTE, 0);
            maxDate.set(Calendar.SECOND, 0);
            maxDate.set(Calendar.MILLISECOND, 0);
            if (mMinDate != null && maxDate.before(mMinDate)) {
                throw new IllegalArgumentException("maxDate has to be after minDate");
            }
        }

        mMaxDate = maxDate;
        resetHomeDate();
        mCurrentOrigin.x = 0;
        invalidate();
    }


    public boolean isShowDistinctWeekendColor() {
        return mShowDistinctWeekendColor;
    }

    public void setShowDistinctWeekendColor(boolean showDistinctWeekendColor) {
        this.mShowDistinctWeekendColor = showDistinctWeekendColor;
        invalidate();
    }

    public void setAutoLimitTime(boolean isAuto) {
        this.mAutoLimitTime = isAuto;
        invalidate();
    }

    public void setLimitTime(int startHour, int endHour) {
        if (endHour <= startHour || startHour < 0 || endHour > 24) {
            throw new IllegalArgumentException("endHour must larger startHour");
        }
        this.mMinTime = startHour;
        this.mMaxTime = endHour;
        int height = (int) ((getHeight() - (mHeaderHeight + mHeaderRowPadding * 2 + mTimeTextHeight / 2 + mHeaderMarginBottom)) / (this.mMaxTime - this.mMinTime));
        if (height > mHourHeight) {
            if (height > mMaxHourHeight)
                mMaxHourHeight = height;
            mNewHourHeight = height;
        }
        invalidate();
    }

    public boolean isShowDistinctPastFutureColor() {
        return mShowDistinctPastFutureColor;
    }

    public void setShowDistinctPastFutureColor(boolean showDistinctPastFutureColor) {
        this.mShowDistinctPastFutureColor = showDistinctPastFutureColor;
        invalidate();
    }

    public boolean isShowNowLine() {
        return mShowNowLine;
    }

    public void setShowNowLine(boolean showNowLine) {
        this.mShowNowLine = showNowLine;
        invalidate();
    }

    public int getNowLineColor() {
        return mNowLineColor;
    }

    public void setNowLineColor(int nowLineColor) {
        this.mNowLineColor = nowLineColor;
        invalidate();
    }

    public int getNowLineThickness() {
        return mNowLineThickness;
    }

    public void setNowLineThickness(int nowLineThickness) {
        this.mNowLineThickness = nowLineThickness;
        invalidate();
    }

    public boolean isHorizontalFlingEnabled() {
        return mHorizontalFlingEnabled;
    }

    public void setHorizontalFlingEnabled(boolean enabled) {
        mHorizontalFlingEnabled = enabled;
    }

    public boolean isVerticalFlingEnabled() {
        return mVerticalFlingEnabled;
    }

    public void setVerticalFlingEnabled(boolean enabled) {
        mVerticalFlingEnabled = enabled;
    }

    public int getAllDayEventHeight() {
        return mAllDayEventHeight;
    }

    public void setAllDayEventHeight(int height) {
        mAllDayEventHeight = height;
    }

    public void setZoomFocusPointEnabled(boolean zoomFocusPointEnabled) {
        mZoomFocusPointEnabled = zoomFocusPointEnabled;
    }

    public boolean isZoomFocusPointEnabled() {
        return mZoomFocusPointEnabled;
    }

    public float getZoomFocusPoint() {
        return mZoomFocusPoint;
    }

    public void setZoomFocusPoint(float zoomFocusPoint) {
        if (0 > zoomFocusPoint || zoomFocusPoint > 1)
            throw new IllegalStateException("The zoom focus point percentage has to be between 0 and 1");
        mZoomFocusPoint = zoomFocusPoint;
    }

    public int getScrollDuration() {
        return mScrollDuration;
    }

    public void setScrollDuration(int scrollDuration) {
        mScrollDuration = scrollDuration;
    }

    Calendar selectedTime = null;
    Calendar endTime = null;
    float startX, startY, currenttX, currenttY;
    int idx = 0;
    DisplayMetrics displayMetrics = new DisplayMetrics();

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        sizeOfWeekView = (mWidthPerDay + mColumnGap) * getNumberOfVisibleDays();
        distanceMin = sizeOfWeekView / offsetValueToSecureScreen;

        mScaleDetector.onTouchEvent(event);
        boolean val = mGestureDetector.onTouchEvent(event);

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (getNumberOfVisibleDays() == 1) {
                mCurrentScrollDirection = Direction.NONE;
            }

            startX = 0;
            startY = 0;
            currenttX = 0;
            currenttY = 0;
            selectedTime = null;
            endTime = null;

            Calendar ss = Calendar.getInstance();
            Calendar ee = Calendar.getInstance();

            if (longPressed) {
                if (mNewEventRect != null)
                    if (mNewEventRect.event != null) {
                        if (!mNewEventRect.event.getStartTime().equals(mNewEventRect.event.getEndTime())) {
                            ss.setTimeInMillis(mNewEventRect.event.getStartTime());
                            ee.setTimeInMillis(mNewEventRect.event.getEndTime());
                            mAddEventClickListener.onAddEventClicked(ss, ee, mNewEventRect.event.getColor());
                        }
                    }
            }

            longPressed = false;
            mNewEventRect = null;
            notifyDatasetChanged();
            mWeekViewDragListener.onWeekViewStopsHandlingTouch();
        }

        if (event.getAction() == MotionEvent.ACTION_UP && !mIsZooming && mCurrentFlingDirection == Direction.NONE) {
            if (mCurrentScrollDirection == Direction.RIGHT || mCurrentScrollDirection == Direction.LEFT) {
                goToNearestOrigin();
            }
            mCurrentScrollDirection = Direction.NONE;
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
            selectedTime = getTimeFromPoint(event.getX(), event.getY());
            startX = event.getX();
            startY = event.getY();

            idx = new Random().nextInt(rainbow.length);

        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (longPressed) {
                if (currenttX != event.getX() || currenttY != event.getY()) {
                    currenttX = event.getX();
                    currenttY = event.getY();
                    if ((mEmptyViewClickListener != null || mAddEventClickListener != null) && event.getX() > mHeaderColumnWidth) {

                        List<EventRect> tempEventRects = new ArrayList<>(mEventRects);
                        mEventRects = new ArrayList<>();
                        if (selectedTime != null) {
                            if (mNewEventRect != null) {
                                tempEventRects.remove(mNewEventRect);
                                mNewEventRect = null;
                            }

                            playSoundEffect(SoundEffectConstants.CLICK);
                            if (mEmptyViewClickListener != null)
                                mEmptyViewClickListener.onEmptyViewClicked(selectedTime);

                            if (mAddEventClickListener != null) {

                                int unroundedMinutes = selectedTime.get(Calendar.MINUTE);
                                int mod = unroundedMinutes % mNewEventTimeResolutionInMinutes;
                                selectedTime.add(Calendar.MINUTE, mod < Math.ceil(mNewEventTimeResolutionInMinutes / 2) ? -mod : (mNewEventTimeResolutionInMinutes - mod));

                                Calendar endTime = getTimeFromPoint(startX, event.getY());
                                if (endTime.before(selectedTime)) {
                                    endTime = (Calendar) selectedTime.clone();
                                    endTime.add(Calendar.MINUTE, 5);
                                } else {
                                    endTime.add(Calendar.MINUTE, mNewEventTimeResolutionInMinutes);
                                }

                                int unroundedEndMinutes = endTime.get(Calendar.MINUTE);
                                int modEnd = unroundedEndMinutes % mNewEventTimeResolutionInMinutes;
                                endTime.add(Calendar.MINUTE, modEnd < Math.ceil(mNewEventTimeResolutionInMinutes / 2) ? -modEnd : (mNewEventTimeResolutionInMinutes - modEnd));

                                this.endTime = endTime;


                                if (event.getY() > getHeight() - 200) {
                                    mCurrentOrigin.y -= 10;
                                    invalidate();
                                } else if (event.getY() < 250) {
                                    mCurrentOrigin.y += 10;
                                    invalidate();
                                }

                                Log.d("height", getHeight() + " " + currenttY);

                                WeekViewEvent newEvent = new WeekViewEvent("", selectedTime.getTimeInMillis(), endTime.getTimeInMillis(), 2, endTime.getTimeInMillis(), idx);

                                int marginTop = mHourHeight * mMinTime;
                                float top = selectedTime.get(Calendar.HOUR_OF_DAY) * 60;
                                top = mHourHeight * top / 60 + mCurrentOrigin.y + mTimeTextHeight / 2 + mEventMarginVertical - marginTop;
                                float bottom = endTime.get(Calendar.HOUR_OF_DAY) * 60;
                                bottom = mHourHeight * bottom / 60 + mCurrentOrigin.y + mTimeTextHeight / 2 - mEventMarginVertical - marginTop;

                                float left = 0;
                                float right = left + mWidthPerDay;

                                if (left < right && left < getWidth()) {
                                    RectF dayRectF = new RectF(left, top, right, bottom);
                                    mNewEventRect = new EventRect(newEvent, newEvent, dayRectF);
                                    tempEventRects.add(mNewEventRect);
                                }
                            }
                        }
                        computePositionOfEvents(tempEventRects);
                        invalidate();
                    }
                }
            }
        }
        return val;
    }

    private void goToNearestOrigin() {
        if (getNumberOfVisibleDays() == 1) {
            double leftDays = mCurrentOrigin.x / (mWidthPerDay + mColumnGap);

            if (mCurrentFlingDirection != Direction.NONE) {
                leftDays = Math.round(leftDays);
            } else if (mCurrentScrollDirection == Direction.LEFT) {
                leftDays = Math.floor(leftDays);
            } else if (mCurrentScrollDirection == Direction.RIGHT) {
                leftDays = Math.ceil(leftDays);
            } else {
                leftDays = Math.round(leftDays);
            }

            Calendar currentDay = Calendar.getInstance();
            currentDay.add(Calendar.DAY_OF_YEAR, -(int) Math.round(leftDays));
            currentDay.set(Calendar.HOUR_OF_DAY, 0);
            currentDay.set(Calendar.MINUTE, 0);
            currentDay.set(Calendar.SECOND, 0);
            currentDay.set(Calendar.MILLISECOND, 0);

            if (currentDay.getTime() != currentDate) {
                currentDate = currentDay.getTime();
                mDateChangeListener.onDateChange(currentDay.get(Calendar.YEAR), currentDay.get(Calendar.MONTH) + 1, currentDay.get(Calendar.DAY_OF_MONTH), true);
            }

            int nearestOrigin = (int) (mCurrentOrigin.x - leftDays * (mWidthPerDay + mColumnGap));

            boolean mayScrollHorizontal = mCurrentOrigin.x - nearestOrigin < getXMaxLimit() && mCurrentOrigin.x - nearestOrigin > getXMinLimit();

            if (mayScrollHorizontal) {
                mScroller.startScroll((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, -nearestOrigin, 0);
                ViewCompat.postInvalidateOnAnimation(WeekView.this);
            }

            if (nearestOrigin != 0 && mayScrollHorizontal) {
                mScroller.forceFinished(true);

                if (getNumberOfVisibleDays() == 1) {
                    mScroller.startScroll((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, -nearestOrigin, 0, (int) (Math.abs(nearestOrigin) / mWidthPerDay * mScrollDuration));
                } else {
                    mScroller.startScroll((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, -nearestOrigin, 0, (int) (Math.abs(nearestOrigin) / mWidthPerDay * (mScrollDuration / 7)));
                }
                ViewCompat.postInvalidateOnAnimation(WeekView.this);
            }
            mCurrentScrollDirection = mCurrentFlingDirection = Direction.NONE;
        } else {
            float beforeScroll = startOriginForScroll;
            boolean isPassed = false;

            if (distanceDone > distanceMin || distanceDone < -distanceMin) {

                if (mCurrentScrollDirection == Direction.LEFT) {
                    startOriginForScroll -= sizeOfWeekView;
                    isPassed = true;
                } else if (mCurrentScrollDirection == Direction.RIGHT) {
                    startOriginForScroll += sizeOfWeekView;
                    isPassed = true;
                }

                boolean mayScrollHorizontal = beforeScroll - startOriginForScroll < getXMaxLimit() && mCurrentOrigin.x - startOriginForScroll > getXMinLimit();

                if (isPassed && mayScrollHorizontal) {
                    mScroller.forceFinished(true);
                    if (mCurrentScrollDirection == Direction.LEFT) {
                        mScroller.startScroll((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, (int) ((beforeScroll - mCurrentOrigin.x) - sizeOfWeekView), 0, 250);
                    } else if (mCurrentScrollDirection == Direction.RIGHT) {
                        mScroller.startScroll((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, (int) (sizeOfWeekView - (mCurrentOrigin.x - beforeScroll)), 0, 250);
                    }
                    ViewCompat.postInvalidateOnAnimation(WeekView.this);
                }

                if (mCurrentFlingDirection != Direction.NONE) {
                    double leftDays = mCurrentOrigin.x / (mWidthPerDay + mColumnGap);
                    leftDays = Math.round(leftDays);

                    Calendar currentDay = Calendar.getInstance();
                    currentDay.add(Calendar.DAY_OF_YEAR, -(int) Math.round(leftDays));
                    currentDay.set(Calendar.HOUR_OF_DAY, 0);
                    currentDay.set(Calendar.MINUTE, 0);
                    currentDay.set(Calendar.SECOND, 0);
                    currentDay.set(Calendar.MILLISECOND, 0);

                    if (currentDay.getTime() != currentDate) {
                        currentDate = currentDay.getTime();
                        mDateChangeListener.onDateChange(currentDay.get(Calendar.YEAR), currentDay.get(Calendar.MONTH) + 1, currentDay.get(Calendar.DAY_OF_MONTH), true);
                    }
                }
            } else {
                mScroller.forceFinished(true);
                if (mCurrentScrollDirection == Direction.LEFT) {
                    mScroller.startScroll((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, (int) beforeScroll - (int) mCurrentOrigin.x, 0, 250);
                } else if (mCurrentScrollDirection == Direction.RIGHT) {
                    mScroller.startScroll((int) mCurrentOrigin.x, (int) mCurrentOrigin.y, (int) beforeScroll - (int) mCurrentOrigin.x, 0, 250);
                }
                ViewCompat.postInvalidateOnAnimation(WeekView.this);
            }
            mCurrentScrollDirection = mCurrentFlingDirection = Direction.NONE;
        }
    }

    public void setDateChangeListener(DateChangeListener listener) {
        this.mDateChangeListener = listener;
    }

    public DateChangeListener getmDateChangeListener() {
        return mDateChangeListener;
    }

    @Override
    public void computeScroll() {
        super.computeScroll();

        if (mScroller.isFinished()) {
            if (mCurrentFlingDirection != Direction.NONE) {
                goToNearestOrigin();
            }
        } else {
            if (mCurrentFlingDirection != Direction.NONE && forceFinishScroll()) {
                goToNearestOrigin();
            } else if (mScroller.computeScrollOffset()) {
                mCurrentOrigin.y = mScroller.getCurrY();
                mCurrentOrigin.x = mScroller.getCurrX();
                ViewCompat.postInvalidateOnAnimation(this);
            }
        }
    }

    private boolean forceFinishScroll() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return mScroller.getCurrVelocity() <= mMinimumFlingVelocity;
        } else {
            return false;
        }
    }

    public void goToToday() {
        startOriginForScroll = mCurrentOrigin.x;
        Calendar today = Calendar.getInstance();
        goToDate(today);
    }

    public void setDivideNumberOfOffsetToSecureScreen(int value) {
        offsetValueToSecureScreen = value;
    }

    public float getWeekViewHeightWithoutDateNorHour() {
        return getHeight() - (mHeaderHeight + mHeaderRowPadding * 2 + mHeaderMarginBottom);
    }

    public void goToDate(Calendar date) {
        mScroller.forceFinished(true);
        mCurrentScrollDirection = mCurrentFlingDirection = Direction.NONE;

        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        if (!df.format(currentDate).equals(df.format(date.getTime()))) {
            mRefreshEvents = true;
        }

        if (mAreDimensionsInvalid) {
            {
                mScrollToDay = date;
                currentDate = date.getTime();
                return;
            }
        }

        mCurrentOrigin.x = -daysBetween(mHomeDate, date) * (mWidthPerDay + mColumnGap);
        startOriginForScroll = mCurrentOrigin.x;

        invalidate();
    }

    public void notifyDatasetChanged() {
        mRefreshEvents = true;
        mEventRects = null;
        invalidate();
    }


    public void goToHour(double hour) {
        if (mAreDimensionsInvalid) {
            mScrollToHour = hour;
            return;
        }

        int verticalOffset = 0;
        if (hour > mMaxTime)
            verticalOffset = mHourHeight * (mMaxTime - mMinTime);
        else if (hour > mMinTime)
            verticalOffset = (int) (mHourHeight * hour);

        if (verticalOffset > mHourHeight * (mMaxTime - mMinTime) - getHeight() + mHeaderHeight + mHeaderRowPadding * 2 + mHeaderMarginBottom)
            verticalOffset = (int) (mHourHeight * (mMaxTime - mMinTime) - getHeight() + mHeaderHeight + mHeaderRowPadding * 2 + mHeaderMarginBottom);

        mCurrentOrigin.y = -verticalOffset;
        invalidate();
    }


    public double getFirstVisibleHour() {
        return -mCurrentOrigin.y / mHourHeight;
    }


    public boolean dateIsValid(Calendar day) {
        if (mMinDate != null && day.before(mMinDate)) {
            return false;
        }
        if (mMaxDate != null && day.after(mMaxDate)) {
            return false;
        }
        return true;
    }

    public interface AddEventClickListener {
        void onAddEventClicked(Calendar startTime, Calendar endTime, int color);
    }


    public interface EventClickListener {
        void onEventClick(WeekViewEvent event, RectF eventRect);
    }

    public interface EventLongPressListener {

        void onEventLongPress(WeekViewEvent event, RectF eventRect);
    }

    public interface EmptyViewClickListener {
        void onEmptyViewClicked(Calendar date);
    }

    public interface EmptyViewLongPressListener {

        void onEmptyViewLongPress(Calendar time);
    }

    public interface ScrollListener {
        void onFirstVisibleDayChanged(Calendar newFirstVisibleDay, Calendar oldFirstVisibleDay);
    }

    private class WeekViewGestureListener implements ScaleGestureDetector.OnScaleGestureListener {
        float mFocusedPointY;

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            mIsZooming = false;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            mIsZooming = true;
            goToNearestOrigin();

            if (mZoomFocusPointEnabled) {
                mFocusedPointY = (getHeight() - mHeaderHeight - mHeaderRowPadding * 2 - mHeaderMarginBottom) * mZoomFocusPoint;
            } else {
                mFocusedPointY = detector.getFocusY();
            }

            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            final float scale = detector.getScaleFactor();

            mNewHourHeight = Math.round(mHourHeight * scale);

            float diffY = mFocusedPointY - mCurrentOrigin.y;
            diffY = diffY * scale - diffY;
            mCurrentOrigin.y -= diffY;

            invalidate();
            return true;
        }
    }

    public interface WeekViewDragListener {
        void onWeekViewStartsHandlingTouch();

        void onWeekViewStopsHandlingTouch();
    }

    public interface DateChangeListener {
        void onDateChange(int year, int month, int day, boolean smoothScroll);
    }
}
