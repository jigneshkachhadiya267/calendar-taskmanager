package com.nestcode.taskmanager.view.weekview;

import java.util.Calendar;


public interface DateTimeInterpreter {
    public String interpretDate(Calendar date);
    public String interpretTime(int hour, int minutes);
}
