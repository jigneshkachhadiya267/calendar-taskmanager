package com.nestcode.taskmanager.view.other;

public interface OnColorChangedListener {
    void onColorChanged(int c);
}
