package com.nestcode.taskmanager.view.weekview;

import java.util.Calendar;


public class WeekViewUtil {

    public static boolean isSameDay(Calendar dateOne, Calendar dateTwo) {
        return dateOne.get(Calendar.YEAR) == dateTwo.get(Calendar.YEAR) && dateOne.get(Calendar.DAY_OF_YEAR) == dateTwo.get(Calendar.DAY_OF_YEAR);
    }

    public static Calendar today() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        return today;
    }

    public static boolean isSameDayAndHour(Calendar dateOne, Calendar dateTwo) {

        if (dateTwo != null) {
            return isSameDay(dateOne, dateTwo) && dateOne.get(Calendar.HOUR_OF_DAY) == dateTwo.get(Calendar.HOUR_OF_DAY);
        }
        return false;
    }

    public static int daysBetween(Calendar dateOne, Calendar dateTwo) {
        return (int) ((dateTwo.getTimeInMillis() + dateTwo.getTimeZone().getOffset(dateTwo.getTimeInMillis()) - (dateOne.getTimeInMillis() + dateOne.getTimeZone().getOffset(dateOne.getTimeInMillis()))) / (1000 * 60 * 60 * 24f));
    }

    public static int getPassedMinutesInDay(Calendar date) {
        return getPassedMinutesInDay(date.get(Calendar.HOUR_OF_DAY), date.get(Calendar.MINUTE));
    }

    public static int getPassedMinutesInDay(int hour, int minute) {
        return hour * 60 + minute;
    }
}
