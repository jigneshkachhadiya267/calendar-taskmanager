package com.nestcode.taskmanager.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.nestcode.taskmanager.R;
import com.nestcode.taskmanager.view.other.LineColorPicker;
import com.nestcode.taskmanager.view.other.OnColorChangedListener;
import com.nestcode.taskmanager.view.weekview.WeekViewEvent;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static com.nestcode.taskmanager.activity.BaseActivity.df;
import static com.nestcode.taskmanager.activity.BaseActivity.dpToPx;
import static com.nestcode.taskmanager.activity.BaseActivity.fdf;
import static com.nestcode.taskmanager.activity.BaseActivity.tf;

public class AddEventActivity extends AppCompatActivity {

    enum State {
        EXPANDED, COLLAPSED
    }

    ImageView iv_back;
    TextView tv_event_date;
    TextView tv_start_time;
    TextView tv_end_time;
    TextView tv_repeat_end_time;
    TextView tv_add_event;

    TabLayout tab_frequency;
    CheckBox cb_consumer_location, cb_virtual;

    LinearLayout ll_color;
    LineColorPicker ll_colorpicker;
    View v_current_color;

    String startTime;
    String endTime;
    String repeatEndTime;
    int color;

    private int mYear, mMonth, mDay;

    Date startDate, endDate, repeatEndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_event);

        final int[] rainbow = getResources().getIntArray(R.array.rainbow);

        startTime = getIntent().getStringExtra("startTime");
        endTime = getIntent().getStringExtra("endTime");
        color = getIntent().getIntExtra("color", 0);

        iv_back = findViewById(R.id.iv_back);
        ll_color = findViewById(R.id.ll_color);
        ll_colorpicker = findViewById(R.id.ll_colorpicker);
        v_current_color = findViewById(R.id.v_current_color);

        tv_event_date = findViewById(R.id.tv_event_date);
        tv_start_time = findViewById(R.id.tv_start_time);
        tv_end_time = findViewById(R.id.tv_end_time);
        tv_repeat_end_time = findViewById(R.id.tv_repeat_end_date);
        tv_add_event = findViewById(R.id.tv_add_event);
        tab_frequency = findViewById(R.id.tab_frequency);

        cb_consumer_location = findViewById(R.id.cb_location);
        cb_virtual = findViewById(R.id.cb_virtual);

        ll_colorpicker.setSelectedColor(color);
        v_current_color.setBackgroundColor(rainbow[color]);
        tab_frequency.getTabAt(0).select();

        ll_colorpicker.setTag("COLLAPSED");

        try {
            startDate = fdf.parse(startTime);
            endDate = fdf.parse(endTime);

            String date = df.format(startDate);
            String startTime = tf.format(startDate);
            String endTime = tf.format(endDate);

            tv_event_date.setText(date);
            tv_start_time.setText(startTime);
            tv_end_time.setText(endTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ll_colorpicker.setOnColorChangedListener(new OnColorChangedListener() {
            @Override
            public void onColorChanged(int c) {
                v_current_color.setBackgroundColor(c);
                toggleView(dpToPx(33), dpToPx(0));
                ll_colorpicker.setTag("COLLAPSED");

                for (int i = 0; i < rainbow.length; i++) {
                    if (c == rainbow[i]) {
                        color = i;
                    }
                }
            }
        });

        tv_repeat_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                c.setTime(endDate);
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(AddEventActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar c = Calendar.getInstance();
                                c.set(Calendar.YEAR, year);
                                c.set(Calendar.MONTH, monthOfYear);
                                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                c.set(Calendar.HOUR_OF_DAY, 23);
                                c.set(Calendar.MINUTE, 59);

                                repeatEndDate = c.getTime();
                                String repeatEndTime = df.format(repeatEndDate);
                                tv_repeat_end_time.setText(repeatEndTime);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        tv_event_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                c.setTime(startDate);
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(AddEventActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                Calendar startCal = Calendar.getInstance();
                                startCal.setTime(startDate);
                                startCal.set(Calendar.YEAR, year);
                                startCal.set(Calendar.MONTH, monthOfYear);
                                startCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                                Calendar endCal = Calendar.getInstance();
                                endCal.setTime(endDate);
                                endCal.set(Calendar.YEAR, year);
                                endCal.set(Calendar.MONTH, monthOfYear);
                                endCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                startDate = startCal.getTime();
                                endDate = endCal.getTime();

                                String startTime = df.format(startDate);
                                tv_event_date.setText(startTime);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
                datePickerDialog.show();
            }
        });


        tv_start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                mcurrentTime.setTime(startDate);
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        Calendar startCalender = Calendar.getInstance();
                        startCalender.setTime(startDate);
                        startCalender.set(Calendar.HOUR_OF_DAY, selectedHour);
                        startCalender.set(Calendar.MINUTE, selectedMinute);

                        startDate = startCalender.getTime();
                        tv_start_time.setText(tf.format(startDate));
                    }
                }, hour, minute, false);
                mTimePicker.setTitle("Select Start Time");
                mTimePicker.show();
            }
        });


        tv_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                mcurrentTime.setTime(endDate);
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        Calendar endCalender = Calendar.getInstance();
                        endCalender.setTime(endDate);
                        endCalender.set(Calendar.HOUR_OF_DAY, selectedHour);
                        endCalender.set(Calendar.MINUTE, selectedMinute);

                        endDate = endCalender.getTime();
                        tv_end_time.setText(tf.format(endDate));
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Start Time");
                mTimePicker.show();
            }
        });

        ll_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (State.valueOf(ll_colorpicker.getTag().toString())) {
                    case EXPANDED:
                        toggleView(dpToPx(33), dpToPx(0));
                        ll_colorpicker.setTag("COLLAPSED");
                        break;

                    case COLLAPSED:
                        toggleView(0, dpToPx(33));
                        ll_colorpicker.setTag("EXPANDED");
                        break;
                }
            }
        });


        tab_frequency.setOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    repeatEndDate = null;
                    repeatEndTime = "N/A";
                    tv_repeat_end_time.setText(repeatEndTime);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tv_add_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String event = "";
                if (cb_consumer_location.isChecked()) {
                    event = "Consumer Location";
                }

                if (cb_virtual.isChecked()) {
                    if (event.isEmpty()) {
                        event = "Virtual";
                    } else {
                        event += ", Virtual";
                    }
                }

                if (startDate.before(Calendar.getInstance().getTime())) {
                    Toast.makeText(AddEventActivity.this, "Start Time can not be Less then Current Time.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (startDate.after(endDate)) {
                    Toast.makeText(AddEventActivity.this, "Start Time can not be greater then End Time.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (startDate.equals(endDate)) {
                    Toast.makeText(AddEventActivity.this, "Start Time and End Time Can not be Equal.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!cb_virtual.isChecked() && !cb_consumer_location.isChecked()) {
                    Toast.makeText(AddEventActivity.this, "Please select any one Type Of Service.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (tab_frequency.getSelectedTabPosition() != 0 && repeatEndDate == null) {
                    Toast.makeText(AddEventActivity.this, "Please Select Repeat End Date.", Toast.LENGTH_SHORT).show();
                    return;
                }

                Calendar startCal = Calendar.getInstance();
                startCal.setTime(startDate);

                Calendar endCal = Calendar.getInstance();
                endCal.setTime(endDate);


                if (endCal.get(Calendar.HOUR_OF_DAY) == 0){
                    endCal.add(Calendar.MINUTE, -1);
                }

                Calendar repeatEndCal = Calendar.getInstance();
                if (repeatEndDate != null) {
                    repeatEndCal.setTime(repeatEndDate);
                    repeatEndCal.set(Calendar.HOUR_OF_DAY, 23);
                    repeatEndCal.set(Calendar.MINUTE, 59);
                    repeatEndCal.set(Calendar.SECOND, 59);

                    if (repeatEndCal.getTimeInMillis() < endCal.getTimeInMillis()) {
                        Toast.makeText(AddEventActivity.this, "Repeat End Time must be Greater then Event End Time", Toast.LENGTH_SHORT).show();
                        return;
                    }

                }

                WeekViewEvent weekViewEvent = new WeekViewEvent();
                weekViewEvent.setTos(event);
                weekViewEvent.setStartTime(startCal.getTimeInMillis());
                weekViewEvent.setEndTime(endCal.getTimeInMillis());

                if (repeatEndDate != null) {
                    weekViewEvent.setRepeatEnds(repeatEndCal.getTimeInMillis());
                }else{
                    weekViewEvent.setRepeatEnds(0L);
                }
                weekViewEvent.setRepeatType(tab_frequency.getSelectedTabPosition());
                weekViewEvent.setColor(color);
                try {
                    weekViewEvent.save();
                    finish();
                } catch (Exception e) {
                    Toast.makeText(AddEventActivity.this, "Problem adding Event. Please try again.", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    finish();
                }
            }
        });
    }

    public void toggleView(float start, float end) {
        AnimatorSet set = new AnimatorSet();
        ValueAnimator a = ObjectAnimator.ofFloat(start, end);
        set.play(a);
        a.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams params = ll_colorpicker.getLayoutParams();
                params.height = (int) (float) animation.getAnimatedValue();
                ll_colorpicker.setLayoutParams(params);
            }
        });
        set.setDuration(400L);
        set.setInterpolator(new AccelerateDecelerateInterpolator());
        set.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.slide_out);
    }
}
