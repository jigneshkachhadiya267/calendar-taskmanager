package com.nestcode.taskmanager.activity;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nestcode.taskmanager.R;
import com.nestcode.taskmanager.view.calendarview.Calendar;
import com.nestcode.taskmanager.view.calendarview.CalendarView;
import com.nestcode.taskmanager.view.weekview.WeekViewEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.nestcode.taskmanager.activity.IndexActivity.CalenderType.DAY;
import static com.nestcode.taskmanager.activity.IndexActivity.CalenderType.MONTH;
import static com.nestcode.taskmanager.activity.IndexActivity.CalenderType.WEEK;
import static com.nestcode.taskmanager.activity.IndexActivity.CalenderType.YEAR;

public class IndexActivity extends BaseActivity implements CalendarView.OnCalendarSelectListener, CalendarView.OnCalendarLongClickListener, CalendarView.OnYearChangeListener, CalendarView.OnViewChangeListener, CalendarView.OnMonthChangeListener {


    enum CalenderType {
        DAY, WEEK, MONTH, YEAR
    }

    TextView mTextCurrentDay;
    RelativeLayout mRelativeTool;
    private int mYear;
    ImageView iv_day, iv_week, iv_month, iv_year;
    public CalenderType cType = DAY;
    Set<WeekViewEvent> allEvents = new HashSet<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_index;
    }

    @Override
    public List<? extends WeekViewEvent> onWeekViewMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> dbEvents = WeekViewEvent.listAll(WeekViewEvent.class);

        allEvents.clear();

        for (int i = 0; i < dbEvents.size(); i++) {
            WeekViewEvent e = dbEvents.get(i);
            if (e.getRepeatType() == 0) {
                java.util.Calendar start = java.util.Calendar.getInstance();
                start.setTimeInMillis(e.getStartTime());

                java.util.Calendar end = java.util.Calendar.getInstance();
                end.setTimeInMillis(e.getEndTime());

                if (start.get(java.util.Calendar.YEAR) == newYear && end.get(java.util.Calendar.MONTH) == (newMonth - 1)) {
                    allEvents.add(e);
                }
            } else if (e.getRepeatType() == 1) {

                java.util.Calendar startTime = java.util.Calendar.getInstance();
                java.util.Calendar endTime = java.util.Calendar.getInstance();

                startTime.setTimeInMillis(e.getStartTime());
                endTime.setTimeInMillis(e.getEndTime());

                java.util.Calendar selectedCalender = java.util.Calendar.getInstance();
                selectedCalender.set(java.util.Calendar.YEAR, newYear);
                selectedCalender.set(java.util.Calendar.MONTH, newMonth - 1);

                int dayEnd = selectedCalender.getActualMaximum(java.util.Calendar.DATE);

                for (int j = 1; j <= dayEnd; j++) {
                    startTime.set(java.util.Calendar.DATE, j);
                    endTime.set(java.util.Calendar.DATE, j);

                    startTime.set(java.util.Calendar.MONTH, newMonth - 1);
                    endTime.set(java.util.Calendar.MONTH, newMonth - 1);

                    startTime.set(java.util.Calendar.YEAR, newYear);
                    endTime.set(java.util.Calendar.YEAR, newYear);

                    if (startTime.getTimeInMillis() >= e.getStartTime()) {
                        if (endTime.getTimeInMillis() <= e.getRepeatEnds()) {
                            WeekViewEvent dailyEvent = new WeekViewEvent();
                            dailyEvent.setId(e.getId());
                            dailyEvent.setTos(e.getTos());
                            dailyEvent.setRepeatType(e.getRepeatType());
                            dailyEvent.setColor(e.getColor());
                            dailyEvent.setStartTime(startTime.getTimeInMillis());
                            dailyEvent.setEndTime(endTime.getTimeInMillis());
                            allEvents.add(dailyEvent);
                        }
                    }
                }
            } else if (e.getRepeatType() == 2) {
                java.util.Calendar startTimeWeekly = java.util.Calendar.getInstance();
                startTimeWeekly.setTimeInMillis(e.getStartTime());

                java.util.Calendar endTimeWeekly = java.util.Calendar.getInstance();
                endTimeWeekly.setTimeInMillis(e.getEndTime());

                java.util.Calendar selectedCalenderWeekday = java.util.Calendar.getInstance();
                selectedCalenderWeekday.set(java.util.Calendar.YEAR, newYear);
                selectedCalenderWeekday.set(java.util.Calendar.MONTH, newMonth - 1);

                DateFormat dfff = new SimpleDateFormat("EEE");
                String day = dfff.format(startTimeWeekly.getTime());

                int dayEndWeekly = selectedCalenderWeekday.getActualMaximum(java.util.Calendar.DATE);

                for (int j = 1; j <= dayEndWeekly; j++) {
                    startTimeWeekly.set(java.util.Calendar.DATE, j);
                    endTimeWeekly.set(java.util.Calendar.DATE, j);

                    startTimeWeekly.set(java.util.Calendar.MONTH, newMonth - 1);
                    endTimeWeekly.set(java.util.Calendar.MONTH, newMonth - 1);

                    startTimeWeekly.set(java.util.Calendar.YEAR, newYear);
                    endTimeWeekly.set(java.util.Calendar.YEAR, newYear);

                    if (day.equalsIgnoreCase(dfff.format(startTimeWeekly.getTime()))) {
                        if (startTimeWeekly.getTimeInMillis() >= e.getStartTime()) {
                            if (endTimeWeekly.getTimeInMillis() <= e.getRepeatEnds()) {
                                WeekViewEvent dailyEvent = new WeekViewEvent();
                                dailyEvent.setId(e.getId());
                                dailyEvent.setTos(e.getTos());
                                dailyEvent.setRepeatType(e.getRepeatType());
                                dailyEvent.setColor(e.getColor());
                                dailyEvent.setStartTime(startTimeWeekly.getTimeInMillis());
                                dailyEvent.setEndTime(endTimeWeekly.getTimeInMillis());
                                allEvents.add(dailyEvent);
                            }
                        }
                    }
                }
            } else if (e.getRepeatType() == 3) {

                java.util.Calendar startTimeMonthly = java.util.Calendar.getInstance();
                startTimeMonthly.setTimeInMillis(e.getStartTime());

                java.util.Calendar endTimeMonthly = java.util.Calendar.getInstance();
                endTimeMonthly.setTimeInMillis(e.getEndTime());

                startTimeMonthly.set(java.util.Calendar.MONTH, newMonth - 1);
                endTimeMonthly.set(java.util.Calendar.MONTH, newMonth - 1);

                startTimeMonthly.set(java.util.Calendar.YEAR, newYear);
                endTimeMonthly.set(java.util.Calendar.YEAR, newYear);

                if (startTimeMonthly.getTimeInMillis() >= e.getStartTime()) {

                    java.util.Calendar tempCal = java.util.Calendar.getInstance();
                    tempCal.setTimeInMillis(e.getStartTime());

                    e.setStartTime(startTimeMonthly.getTimeInMillis());
                    e.setEndTime(endTimeMonthly.getTimeInMillis());

                    if (startTimeMonthly.get(java.util.Calendar.DATE) == tempCal.get(java.util.Calendar.DATE)) {
                        if (endTimeMonthly.getTimeInMillis() <= e.getRepeatEnds()) {
                            allEvents.add(e);
                        }
                    }
                }
            } else if (e.getRepeatType() == 4) {
                java.util.Calendar startTimeYearly = java.util.Calendar.getInstance();
                startTimeYearly.setTimeInMillis(e.getStartTime());

                java.util.Calendar endTimeYearly = java.util.Calendar.getInstance();
                endTimeYearly.setTimeInMillis(e.getEndTime());

                startTimeYearly.set(java.util.Calendar.YEAR, newYear);
                endTimeYearly.set(java.util.Calendar.YEAR, newYear);

                if (startTimeYearly.getTimeInMillis() >= e.getStartTime()) {
                    e.setStartTime(startTimeYearly.getTimeInMillis());
                    e.setEndTime(endTimeYearly.getTimeInMillis());
                    if (endTimeYearly.getTimeInMillis() <= e.getRepeatEnds()) {
                        allEvents.add(e);
                    }
                }
            }
        }
        return new ArrayList<>(allEvents);
    }

    @Override
    public void onMonthChange(int year, int month) {
        Log.d("WeekDayEvent For: ", "---------------------------------" + getStartDate().getTime().toString() + " " + getEndDate().getTime().toString() + "---------------------------------");
    }

    @Override
    protected void eventAdded() {
        getWeekView().notifyDatasetChanged();
    }

    @Override
    protected void initView() {
        setStatusBarDarkMode();
        mRelativeTool = (RelativeLayout) findViewById(R.id.rl_tool);
        mCalendarView = (CalendarView) findViewById(R.id.calendarView);
        mTextCurrentDay = (TextView) findViewById(R.id.tv_current_day);
        iv_day = findViewById(R.id.iv_day);
        iv_week = findViewById(R.id.iv_week);
        iv_month = findViewById(R.id.iv_month);
        iv_year = findViewById(R.id.iv_year);

        findViewById(R.id.fl_current).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCalendarView.scrollToCurrent();
                iv_day.performClick();
            }
        });

        iv_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCalendarView.setLayoutMode(CalendarView.LayoutMode.WEEK);
                mCalendarView.closeYearSelectLayout();
                cType = DAY;

                ll_rvItems.setVisibility(View.GONE);

                mTextMonthDay.setText(mCalendarView.getSelectedCalendar().getMonth() + "/" + mCalendarView.getSelectedCalendar().getYear());
                getWeekView().setNumberOfVisibleDays(1);
                weekView.goToHour(java.util.Calendar.getInstance().get(java.util.Calendar.HOUR_OF_DAY));
                setSelectedViewType();
            }
        });

        iv_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCalendarView.closeYearSelectLayout();
                mCalendarView.setLayoutMode(CalendarView.LayoutMode.NONE);
                cType = WEEK;
                getWeekView().setNumberOfVisibleDays(7);

                ll_rvItems.setVisibility(View.GONE);

                mTextMonthDay.setText(mCalendarView.getSelectedCalendar().getMonth() + "/" + mCalendarView.getSelectedCalendar().getYear());
                weekView.goToHour(8);
                setSelectedViewType();
            }
        });


        iv_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCalendarView.setLayoutMode(CalendarView.LayoutMode.MONTH);

                cType = MONTH;
                mTextMonthDay.setText(mCalendarView.getSelectedCalendar().getMonth() + "/" + mCalendarView.getSelectedCalendar().getYear());

                ll_rvItems.setVisibility(View.VISIBLE);

                mCalendarView.closeYearSelectLayout();
                getWeekView().setNumberOfVisibleDays(1);
                setSelectedViewType();
            }
        });


        iv_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCalendarView.setLayoutMode(CalendarView.LayoutMode.NONE);
                cType = YEAR;
                mTextMonthDay.setText(mCalendarView.getSelectedCalendar().getYear() + "");
                mCalendarView.showYearSelectLayout(mYear);

                ll_rvItems.setVisibility(View.GONE);

                getWeekView().setNumberOfVisibleDays(1);
                setSelectedViewType();
            }
        });

        mCalendarView.setOnViewChangeListener(this);
        mCalendarView.setOnCalendarSelectListener(this);
        mCalendarView.setOnYearChangeListener(this);
        mCalendarView.setOnMonthChangeListener(this);
        mCalendarView.setOnWeekChangeListener(new CalendarView.OnWeekChangeListener() {
            @Override
            public void onWeekChange(List<Calendar> weekCalendars) {

            }
        });
        mCalendarView.setOnCalendarLongClickListener(this, false);
        mYear = mCalendarView.getCurYear();
        mTextMonthDay.setText(mCalendarView.getCurMonth() + "/" + mCalendarView.getCurYear());
        mTextCurrentDay.setText(String.valueOf(mCalendarView.getCurDay()));
    }

    public void setSelectedViewType() {

        iv_day.setBackgroundColor(Color.WHITE);
        iv_week.setBackgroundColor(Color.WHITE);
        iv_month.setBackgroundColor(Color.WHITE);
        iv_year.setBackgroundColor(Color.WHITE);

        if (cType == DAY) {
            iv_day.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccentLight));
        } else if (cType == WEEK) {
            iv_week.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccentLight));
        } else if (cType == MONTH) {
            iv_month.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccentLight));
        } else if (cType == YEAR) {
            iv_year.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccentLight));
        }

        java.util.Calendar selectedDay = java.util.Calendar.getInstance();
        selectedDay.set(java.util.Calendar.YEAR, mCalendarView.getSelectedCalendar().getYear());
        selectedDay.set(java.util.Calendar.MONTH, mCalendarView.getSelectedCalendar().getMonth() - 1);
        selectedDay.set(java.util.Calendar.DAY_OF_MONTH, mCalendarView.getSelectedCalendar().getDay());

        if (weekView.getNumberOfVisibleDays() != 1) {
            selectedDay.set(java.util.Calendar.DAY_OF_WEEK, 1);
        }
        getWeekView().goToDate(selectedDay);
    }

    @Override
    protected void initData() {}

    @Override
    public void onCalendarOutOfRange(Calendar calendar) {
    }

    @Override
    public void onViewChange(boolean isMonthView) {
        if (isMonthView) {
            cType = MONTH;
            mCalendarView.setLayoutMode(CalendarView.LayoutMode.MONTH);
            ll_rvItems.setVisibility(View.VISIBLE);
        } else {
            if (getWeekView().getNumberOfVisibleDays() == 7) {
                cType = WEEK;
                mCalendarView.setLayoutMode(CalendarView.LayoutMode.NONE);
                weekView.goToHour(8);
            } else {
                cType = DAY;
                weekView.goToHour(8);
                mCalendarView.setLayoutMode(CalendarView.LayoutMode.WEEK);
            }
        }
        setSelectedViewType();
    }

    @Override
    public void onCalendarSelect(Calendar calendar, boolean isClick) {
        mTextMonthDay.setText(calendar.getMonth() + "/" + calendar.getYear());
        java.util.Calendar selectedDay = java.util.Calendar.getInstance();
        selectedDay.set(java.util.Calendar.YEAR, calendar.getYear());
        selectedDay.set(java.util.Calendar.MONTH, calendar.getMonth() - 1);
        selectedDay.set(java.util.Calendar.DAY_OF_MONTH, calendar.getDay());
        getWeekView().goToDate(selectedDay);
        setSelectedViewType();

        adapter.showEvents(selectedDay.getTime());
        mSelectedDay.setText(df.format(selectedDay.getTime()));
        mYear = calendar.getYear();

    }

    @Override
    public void onCalendarLongClickOutOfRange(Calendar calendar) {
    }

    @Override
    public void onCalendarLongClick(Calendar calendar) {
        Log.e("onDateLongClick", "  -- " + calendar.getDay() + "  --  " + calendar.getMonth());
    }

    @Override
    public void onYearChange(int year) {
        mTextMonthDay.setText(String.valueOf(year));
    }
}
