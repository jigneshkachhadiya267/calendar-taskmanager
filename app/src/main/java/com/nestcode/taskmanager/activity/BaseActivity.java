package com.nestcode.taskmanager.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nestcode.taskmanager.R;
import com.nestcode.taskmanager.adapter.EventsAdapter;
import com.nestcode.taskmanager.view.calendarview.CalendarView;
import com.nestcode.taskmanager.view.weekview.DateTimeInterpreter;
import com.nestcode.taskmanager.view.weekview.MonthLoader;
import com.nestcode.taskmanager.view.weekview.WeekView;
import com.nestcode.taskmanager.view.weekview.WeekViewEvent;
import com.orm.SugarContext;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public abstract class BaseActivity extends AppCompatActivity implements WeekView.EventClickListener, MonthLoader.MonthChangeListener, WeekView.EventLongPressListener, WeekView.EmptyViewClickListener, WeekView.AddEventClickListener, WeekView.WeekViewDragListener {

    private static boolean isMiUi = false;
    public static final int ADD_EVENT = 221;
    public WeekView weekView;
    public RecyclerView rvEvents;
    public LinearLayout ll_rvItems;
    public CalendarView mCalendarView;
    public TextView mTextMonthDay, mSelectedDay;
    public EventsAdapter adapter;

    public static DateFormat fdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ssa");
    public static DateFormat df = new SimpleDateFormat("dd MMM, yyyy");
    public static DateFormat tf = new SimpleDateFormat("hh:mm a");
    public static DateFormat tfs = new SimpleDateFormat("h:mm a");


    protected void initWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected abstract int getLayoutId();

    protected abstract void initView();

    protected abstract void initData();

    protected abstract void eventAdded();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWindow();

        SugarContext.init(this);
        setContentView(getLayoutId());

        final int[] rainbow = getResources().getIntArray(R.array.rainbow);


        weekView = (WeekView) findViewById(R.id.weekView);
        rvEvents = findViewById(R.id.rvTasks);
        mTextMonthDay = findViewById(R.id.tv_month_day);
        ll_rvItems = findViewById(R.id.ll_rvTasks);
        mSelectedDay = findViewById(R.id.tv_selected_date);
        adapter = new EventsAdapter(new ArrayList<WeekViewEvent>(), this, weekView);

        rvEvents.setLayoutManager(new LinearLayoutManager(this));
        rvEvents.setAdapter(adapter);

        initView();
        initData();

        weekView.setOnEventClickListener(this);
        weekView.setMonthChangeListener(this);
        weekView.setEventLongPressListener(this);
        weekView.setEmptyViewClickListener(this);
        weekView.setAddEventClickListener(this);
        weekView.setmWeekViewDragListener(this);
        weekView.setDataUpdatedListener(new WeekView.CurrentDataUpdatedListener() {
            @Override
            public void onCurrentDataUpdated(List<? extends WeekViewEvent> mCurrentPeriodEvents) {
                Map<String, com.nestcode.taskmanager.view.calendarview.Calendar> map = new HashMap<>();

                for (int i = 0; i < mCurrentPeriodEvents.size(); i++) {
                    WeekViewEvent e = mCurrentPeriodEvents.get(i);
                    Calendar start = Calendar.getInstance();
                    start.setTimeInMillis(e.getStartTime());

                    com.nestcode.taskmanager.view.calendarview.Calendar schemeCalender = getSchemeCalendar(start.get(Calendar.YEAR), start.get(Calendar.MONTH) + 1, start.get(Calendar.DATE));

                    if (map.containsKey(schemeCalender.toString())) {
                        com.nestcode.taskmanager.view.calendarview.Calendar onDate = map.get(schemeCalender.toString());
                        com.nestcode.taskmanager.view.calendarview.Calendar.Scheme scheme = new com.nestcode.taskmanager.view.calendarview.Calendar.Scheme();
                        scheme.setScheme(" ");
                        scheme.setShcemeColor(rainbow[e.getColor()]);
                        if (onDate != null) {
                            onDate.getSchemes().add(scheme);
                        }
                        map.put(getSchemeCalendar(start.get(Calendar.YEAR), start.get(Calendar.MONTH) + 1, start.get(Calendar.DATE)).toString(), onDate);

                    } else {
                        List<com.nestcode.taskmanager.view.calendarview.Calendar.Scheme> schemes = new ArrayList<>();

                        com.nestcode.taskmanager.view.calendarview.Calendar.Scheme scheme = new com.nestcode.taskmanager.view.calendarview.Calendar.Scheme();
                        scheme.setScheme(" ");
                        scheme.setShcemeColor(rainbow[e.getColor()]);
                        schemes.add(scheme);
                        schemeCalender.setSchemes(schemes);
                        map.put(getSchemeCalendar(start.get(Calendar.YEAR), start.get(Calendar.MONTH) + 1, start.get(Calendar.DATE)).toString(), schemeCalender);
                    }
                }

                mCalendarView.setSchemeDate(map);

                adapter.setEvents(new ArrayList<>(mCurrentPeriodEvents));
                Calendar selectedDay = Calendar.getInstance();
                selectedDay.set(Calendar.YEAR, mCalendarView.getSelectedCalendar().getYear());
                selectedDay.set(Calendar.MONTH, mCalendarView.getSelectedCalendar().getMonth() - 1);
                selectedDay.set(Calendar.DAY_OF_MONTH, mCalendarView.getSelectedCalendar().getDay());
                adapter.showEvents(selectedDay.getTime());
            }
        });
        weekView.setDateChangeListener(new WeekView.DateChangeListener() {
            @Override
            public void onDateChange(int year, int month, int day, boolean smoothScroll) {
                if (weekView.getNumberOfVisibleDays() == 1) {
                    mCalendarView.scrollToCalendar(year, month, day, smoothScroll);
                } else {
                    mTextMonthDay.setText(month + "/" + year);
                }
            }
        });

        weekView.goToHour(8);
        setupDateTimeInterpreter(false);
    }


    private com.nestcode.taskmanager.view.calendarview.Calendar getSchemeCalendar(int year, int month, int day) {
        com.nestcode.taskmanager.view.calendarview.Calendar calendar = new com.nestcode.taskmanager.view.calendarview.Calendar();
        calendar.setYear(year);
        calendar.setMonth(month);
        calendar.setDay(day);
        return calendar;
    }


    @SuppressLint("PrivateApi")
    private void setMIUIStatusBarDarkMode() {
        if (isMiUi) {
            Class<? extends Window> clazz = getWindow().getClass();
            try {
                int darkModeFlag;
                Class<?> layoutParams = Class.forName("android.view.MiuiWindowManager$LayoutParams");
                Field field = layoutParams.getField("EXTRA_FLAG_STATUS_BAR_DARK_MODE");
                darkModeFlag = field.getInt(layoutParams);
                Method extraFlagField = clazz.getMethod("setExtraFlags", int.class, int.class);
                extraFlagField.invoke(getWindow(), darkModeFlag, darkModeFlag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setupDateTimeInterpreter(final boolean shortDate) {
        weekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" dd", Locale.getDefault());

                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour, int minutes) {
                String strMinutes = String.format("%02d", minutes);
                if (hour > 11) {
                    return (hour - 12) + ":" + strMinutes + " PM";
                } else {
                    if (hour == 0) {
                        return "12:" + strMinutes + " AM";
                    } else {
                        return hour + ":" + strMinutes + " AM";
                    }
                }
            }
        });
    }

    @Override
    public void onEventClick(final WeekViewEvent event, RectF eventRect) {
        Calendar startTime = Calendar.getInstance();
        startTime.setTimeInMillis(event.getStartTime());

        if (event.getRepeatType() > 0) {

            AlertDialog.Builder updateDialog = new AlertDialog.Builder(this);
            updateDialog.setTitle("Update Event")
                    .setMessage("Do you want to update or delete event?")
                    .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UpdateEvent(event);
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    event.delete();
                    getWeekView().notifyDatasetChanged();
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = updateDialog.create();
            dialog.show();
            dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#e62e00"));
            dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.GRAY);

        } else {
            if ((Calendar.getInstance().getTime()).before(startTime.getTime())) {
                AlertDialog.Builder updateDialog = new AlertDialog.Builder(this);
                updateDialog.setTitle("Update Event")
                        .setMessage("Do you want to update or delete event?")
                        .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                UpdateEvent(event);
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        event.delete();
                        getWeekView().notifyDatasetChanged();
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = updateDialog.create();
                dialog.show();
                dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#e62e00"));
                dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.GRAY);
            } else {
                Toast.makeText(this, "Event is not Editable.\nTime Elapsed for the event " + event.getTos(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        Log.e("onEventClick", "Clicked " + event.getTos());
    }

    public void UpdateEvent(WeekViewEvent event) {
        Intent editIntent = new Intent(BaseActivity.this, EditEventActivity.class);
        editIntent.putExtra("event", event.getId());
        startActivityForResult(editIntent, ADD_EVENT);
    }

    public WeekView getWeekView() {
        return weekView;
    }

    @Override
    public void onEmptyViewClicked(Calendar date) {

    }

    @Override
    public void onAddEventClicked(Calendar startTime, Calendar endTime, int color) {
        if (Calendar.getInstance().getTime().before(startTime.getTime())) {
            Intent intent = new Intent(BaseActivity.this, AddEventActivity.class);
            intent.putExtra("startTime", fdf.format(startTime.getTime()));
            intent.putExtra("endTime", fdf.format(endTime.getTime()));
            intent.putExtra("color", color);
            startActivityForResult(intent, ADD_EVENT);
            overridePendingTransition(R.anim.slide_in, R.anim.fade_out);
        } else {
            Toast.makeText(this, "Can't Create event on Elapsed Time.", Toast.LENGTH_SHORT).show();
            getWeekView().notifyDatasetChanged();
        }
    }

    static {
        try {
            @SuppressLint("PrivateApi") Class<?> sysClass = Class.forName("android.os.SystemProperties");
            Method getStringMethod = sysClass.getDeclaredMethod("get", String.class);
            String version = (String) getStringMethod.invoke(sysClass, "ro.miui.ui.version.name");
            isMiUi = version.compareTo("V6") >= 0 && Build.VERSION.SDK_INT < 24;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean setMeiZuDarkMode(Window window, boolean dark) {
        boolean result = false;
        if (Build.VERSION.SDK_INT >= 24) {
            return false;
        }
        if (window != null) {
            try {
                WindowManager.LayoutParams lp = window.getAttributes();
                Field darkFlag = WindowManager.LayoutParams.class.getDeclaredField("MEIZU_FLAG_DARK_STATUS_BAR_ICON");
                Field meizuFlags = WindowManager.LayoutParams.class.getDeclaredField("meizuFlags");
                darkFlag.setAccessible(true);
                meizuFlags.setAccessible(true);
                int bit = darkFlag.getInt(null);
                int value = meizuFlags.getInt(lp);
                if (dark) {
                    value |= bit;
                } else {
                    value &= ~bit;
                }
                meizuFlags.setInt(lp, value);
                window.setAttributes(lp);
                result = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ADD_EVENT) {
            eventAdded();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressLint("InlinedApi")
    private int getStatusBarLightMode() {
        int result = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isMiUi) {
                result = 1;
            } else if (setMeiZuDarkMode(getWindow(), true)) {
                result = 2;
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                result = 3;
            }
        }
        return result;
    }


    @SuppressLint("InlinedApi")
    protected void setStatusBarDarkMode() {
        int type = getStatusBarLightMode();
        if (type == 1) {
            setMIUIStatusBarDarkMode();
        } else if (type == 2) {
            setMeiZuDarkMode(getWindow(), true);
        } else if (type == 3) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    public void onWeekViewStartsHandlingTouch() {
        mCalendarView.requestDisallowInterceptTouchEvent(true);
    }

    @Override
    public void onWeekViewStopsHandlingTouch() {
        mCalendarView.requestDisallowInterceptTouchEvent(false);
    }

    public static float dpToPx(float dp) {
        return (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public Calendar getStartDate() {
        Calendar calendar = getGeorgianDate();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar;
    }

    public Calendar getEndDate() {
        Calendar calendar = getStartDate();
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DATE, -1);
        return calendar;
    }

    public java.util.Calendar getGeorgianDate() {
        java.util.Calendar georgean = java.util.Calendar.getInstance();
        georgean.set(java.util.Calendar.YEAR, mCalendarView.getIndexCalender().getYear());
        georgean.set(java.util.Calendar.MONTH, mCalendarView.getIndexCalender().getMonth() - 1);
        georgean.set(java.util.Calendar.DAY_OF_MONTH, mCalendarView.getIndexCalender().getDay());
        return georgean;
    }
}
