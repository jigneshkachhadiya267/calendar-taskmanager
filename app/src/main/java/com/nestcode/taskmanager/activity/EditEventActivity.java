package com.nestcode.taskmanager.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.nestcode.taskmanager.R;
import com.nestcode.taskmanager.view.other.LineColorPicker;
import com.nestcode.taskmanager.view.other.OnColorChangedListener;
import com.nestcode.taskmanager.view.weekview.WeekViewEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.nestcode.taskmanager.activity.BaseActivity.df;
import static com.nestcode.taskmanager.activity.BaseActivity.dpToPx;
import static com.nestcode.taskmanager.activity.BaseActivity.tf;

public class EditEventActivity extends AppCompatActivity {

    enum State {
        EXPANDED, COLLAPSED
    }

    ImageView iv_back;
    TextView tv_event_date;
    TextView tv_start_time;
    TextView tv_end_time;
    TextView tv_repeat_end_time;
    TextView tv_add_event;

    TabLayout tab_frequency;
    CheckBox cb_consumer_location, cb_virtual;

    LinearLayout ll_color;
    LineColorPicker ll_colorpicker;
    View v_current_color;

    private int mYear, mMonth, mDay;


    int color;
    WeekViewEvent event;
    Calendar startDate, endDate, repeatEndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_edit_event);

        final int[] rainbow = getResources().getIntArray(R.array.rainbow);

        iv_back = findViewById(R.id.iv_back);
        ll_color = findViewById(R.id.ll_color);
        ll_colorpicker = findViewById(R.id.ll_colorpicker);
        v_current_color = findViewById(R.id.v_current_color);

        tv_event_date = findViewById(R.id.tv_event_date);
        tv_start_time = findViewById(R.id.tv_start_time);
        tv_end_time = findViewById(R.id.tv_end_time);
        tv_repeat_end_time = findViewById(R.id.tv_repeat_end_date);
        tv_add_event = findViewById(R.id.tv_add_event);

        cb_consumer_location = findViewById(R.id.cb_location);
        cb_virtual = findViewById(R.id.cb_virtual);
        tab_frequency = findViewById(R.id.tab_frequency);

        ll_colorpicker.setTag("COLLAPSED");

        try {

            if (getIntent().getExtras() != null) {
                Long id = getIntent().getLongExtra("event", 0L);
                event = WeekViewEvent.findById(WeekViewEvent.class, id);

                startDate = Calendar.getInstance();
                startDate.setTimeInMillis(event.getStartTime());

                endDate = Calendar.getInstance();
                endDate.setTimeInMillis(event.getEndTime());

                if (event.getRepeatType() != 0) {
                    repeatEndDate = Calendar.getInstance();
                    repeatEndDate.setTimeInMillis(event.getRepeatEnds());
                }

                ll_colorpicker.setSelectedColor(event.getColor());
                color = event.getColor();

                v_current_color.setBackgroundColor(rainbow[event.getColor()]);

                String date = df.format(startDate.getTime());
                String startTime = tf.format(startDate.getTime());
                String endTime = tf.format(endDate.getTime());

                if (event.getRepeatType() != 0) {
                    String repeatEndTime = df.format(repeatEndDate.getTime());
                    tv_repeat_end_time.setText(repeatEndTime);
                }else {
                    tv_repeat_end_time.setText("N/A");
                }


                tab_frequency.getTabAt(event.getRepeatType()).select();

                if (!event.getTos().isEmpty()) {
                    String[] items = event.getTos().split(",");
                    if (items.length == 1) {
                        if (items[0].contains("Consumer")) {
                            cb_consumer_location.setChecked(true);
                            cb_virtual.setChecked(false);
                        } else {
                            cb_consumer_location.setChecked(false);
                            cb_virtual.setChecked(true);
                        }
                    } else {
                        cb_consumer_location.setChecked(true);
                        cb_virtual.setChecked(true);
                    }
                }

                tv_event_date.setText(date);
                tv_start_time.setText(startTime);
                tv_end_time.setText(endTime);
            } else {
                finish();
            }

        } catch (Exception e) {
            finish();
            e.printStackTrace();
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ll_colorpicker.setOnColorChangedListener(new OnColorChangedListener() {
            @Override
            public void onColorChanged(int c) {
                v_current_color.setBackgroundColor(c);
                toggleView(dpToPx(33), dpToPx(0));
                ll_colorpicker.setTag("COLLAPSED");

                for (int i = 0; i < rainbow.length; i++) {
                    if (c == rainbow[i]) {
                        color = i;
                    }
                }
            }
        });

        tv_repeat_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (repeatEndDate == null){
                    repeatEndDate = Calendar.getInstance();
                }
                mYear = repeatEndDate.get(Calendar.YEAR);
                mMonth = repeatEndDate.get(Calendar.MONTH);
                mDay = repeatEndDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(EditEventActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar c = Calendar.getInstance();
                                c.setTime(repeatEndDate.getTime());
                                c.set(Calendar.YEAR, year);
                                c.set(Calendar.MONTH, monthOfYear);
                                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                repeatEndDate = c;
                                String repeatEndTime = df.format(repeatEndDate.getTime());
                                tv_repeat_end_time.setText(repeatEndTime);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        ll_color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (State.valueOf(ll_colorpicker.getTag().toString())) {
                    case EXPANDED:
                        toggleView(dpToPx(33), dpToPx(0));
                        ll_colorpicker.setTag("COLLAPSED");
                        break;

                    case COLLAPSED:
                        toggleView(0, dpToPx(33));
                        ll_colorpicker.setTag("EXPANDED");
                        break;
                }
            }
        });


        tv_event_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYear = startDate.get(Calendar.YEAR);
                mMonth = startDate.get(Calendar.MONTH);
                mDay = startDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(EditEventActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                startDate.set(Calendar.YEAR, year);
                                startDate.set(Calendar.MONTH, monthOfYear);
                                startDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                endDate.set(Calendar.YEAR, year);
                                endDate.set(Calendar.MONTH, monthOfYear);
                                endDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                String startTime = df.format(startDate.getTime());
                                tv_event_date.setText(startTime);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
                datePickerDialog.show();
            }
        });


        tv_start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = startDate.get(Calendar.HOUR_OF_DAY);
                int minute = startDate.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(EditEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        startDate.set(Calendar.HOUR_OF_DAY, selectedHour);
                        startDate.set(Calendar.MINUTE, selectedMinute);

                        tv_start_time.setText(tf.format(startDate.getTime()));
                    }
                }, hour, minute, false);
                mTimePicker.setTitle("Select Start Time");
                mTimePicker.show();
            }
        });


        tv_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = endDate.get(Calendar.HOUR_OF_DAY);
                int minute = endDate.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(EditEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        endDate.set(Calendar.HOUR_OF_DAY, selectedHour);
                        endDate.set(Calendar.MINUTE, selectedMinute);
                        tv_end_time.setText(tf.format(endDate.getTime()));
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Start Time");
                mTimePicker.show();
            }
        });


        tv_add_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String eventt = "";
                if (cb_consumer_location.isChecked()) {
                    eventt = "Consumer Location";
                }

                if (cb_virtual.isChecked()) {
                    if (eventt.isEmpty()) {
                        eventt = "Virtual";
                    } else {
                        eventt += ", Virtual";
                    }
                }


                if (startDate.before(Calendar.getInstance().getTime())) {
                    Toast.makeText(EditEventActivity.this, "Start Time can not be Less then Current Time.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (startDate.after(endDate)) {
                    Toast.makeText(EditEventActivity.this, "Start Time can not be greater then End Time.", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (startDate.equals(endDate)) {
                    Toast.makeText(EditEventActivity.this, "Start Time and End Time Can not be Equal.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!cb_virtual.isChecked() && !cb_consumer_location.isChecked()) {
                    Toast.makeText(EditEventActivity.this, "Please select any one Type Of Service.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (tab_frequency.getSelectedTabPosition() != 0 && repeatEndDate == null) {
                    Toast.makeText(EditEventActivity.this, "Please Select Repeat End Date.", Toast.LENGTH_SHORT).show();
                    return;
                }else if (repeatEndDate != null){
                    repeatEndDate.set(Calendar.HOUR_OF_DAY, 23);
                    repeatEndDate.set(Calendar.MINUTE, 59);
                    repeatEndDate.set(Calendar.SECOND, 59);
                }

                if (tab_frequency.getSelectedTabPosition() != 0 && repeatEndDate.getTimeInMillis() < endDate.getTimeInMillis()) {
                    Toast.makeText(EditEventActivity.this, "Repeat End Time must be Greater then Event End Time", Toast.LENGTH_SHORT).show();
                    return;
                }



                if (endDate.get(Calendar.HOUR_OF_DAY) == 0){
                    endDate.add(Calendar.MINUTE, -1);
                }

                event.setTos(eventt);
                event.setStartTime(startDate.getTimeInMillis());
                event.setEndTime(endDate.getTimeInMillis());

                if (tab_frequency.getSelectedTabPosition() != 0) {
                    event.setRepeatEnds(repeatEndDate.getTimeInMillis());
                } else {
                    event.setRepeatEnds(0L);
                }

                event.setRepeatType(tab_frequency.getSelectedTabPosition());
                event.setColor(color);
                event.save();

                List<WeekViewEvent> items = new ArrayList<WeekViewEvent>(WeekViewEvent.listAll(WeekViewEvent.class));
                Log.d("items", items.size() + "");
                finish();
            }
        });

        tab_frequency.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    repeatEndDate = null;
                    tv_repeat_end_time.setText("N/A");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void toggleView(float start, float end) {
        AnimatorSet set = new AnimatorSet();
        ValueAnimator a = ObjectAnimator.ofFloat(start, end);
        set.play(a);
        a.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams params = ll_colorpicker.getLayoutParams();
                params.height = (int) (float) animation.getAnimatedValue();
                ll_colorpicker.setLayoutParams(params);
            }
        });
        set.setDuration(400L);
        set.setInterpolator(new AccelerateDecelerateInterpolator());
        set.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.slide_out);
    }
}
