package com.nestcode.taskmanager.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.nestcode.taskmanager.R;
import com.nestcode.taskmanager.activity.EditEventActivity;
import com.nestcode.taskmanager.view.weekview.WeekView;
import com.nestcode.taskmanager.view.weekview.WeekViewEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.nestcode.taskmanager.activity.BaseActivity.ADD_EVENT;
import static com.nestcode.taskmanager.activity.BaseActivity.df;
import static com.nestcode.taskmanager.activity.BaseActivity.fdf;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder> {

    private List<WeekViewEvent> events;
    private List<WeekViewEvent> allEvents = new ArrayList<>();
    int[] rainbow;
    private int lastPosition = -1;
    private Activity activity;
    WeekView weekview;

    public EventsAdapter(List<WeekViewEvent> events, Activity context, WeekView weekView) {
        this.events = events;
        this.activity = context;
        this.weekview = weekView;
        rainbow = context.getResources().getIntArray(R.array.rainbow);
    }

    @Override
    public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event, parent, false);
        return new EventsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EventsViewHolder holder, int position) {
        final WeekViewEvent event = events.get(position);

        Calendar startCalendar = Calendar.getInstance();
        Calendar endCalendar = Calendar.getInstance();
        startCalendar.setTimeInMillis(event.getStartTime());
        endCalendar.setTimeInMillis(event.getEndTime());
        holder.viewEventColor.setBackgroundColor(rainbow[event.getColor()]);
        holder.tvTypeOfService.setText(event.getTos());

        String sourceStartString = "<b>Start Time : </b> " + fdf.format(startCalendar.getTime());
        String sourceEndString = "<b>End Time : </b> " + fdf.format(endCalendar.getTime());

        holder.tvStartTime.setText(Html.fromHtml(sourceStartString));
        holder.tvEndTime.setText(Html.fromHtml(sourceEndString));

        setAnimation(holder.itemView, position);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Calendar startTime = Calendar.getInstance();
                startTime.setTimeInMillis(event.getStartTime());

                if (event.getRepeatType() > 0) {

                    AlertDialog.Builder updateDialog = new AlertDialog.Builder(activity);
                    updateDialog.setTitle("Update Event")
                            .setMessage("Do you want to update or delete event?")
                            .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    UpdateEvent(event);
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            event.delete();
                            weekview.notifyDatasetChanged();
                            dialog.dismiss();
                        }
                    });

                    AlertDialog dialog = updateDialog.create();
                    dialog.show();
                    dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#e62e00"));
                    dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.GRAY);

                } else {
                    if ((Calendar.getInstance().getTime()).before(startTime.getTime())) {
                        AlertDialog.Builder updateDialog = new AlertDialog.Builder(activity);
                        updateDialog.setTitle("Update Event")
                                .setMessage("Do you want to update or delete event?")
                                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        UpdateEvent(event);
                                    }
                                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).setNeutralButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                event.delete();
                                weekview.notifyDatasetChanged();
                                dialog.dismiss();
                            }
                        });

                        AlertDialog dialog = updateDialog.create();
                        dialog.show();
                        dialog.getButton(DialogInterface.BUTTON_NEUTRAL).setTextColor(Color.parseColor("#e62e00"));
                        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.GRAY);
                    } else {
                        Toast.makeText(activity, "Event is not Editable.\nTime Elapsed for the event " + event.getTos(), Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });
    }


    public void UpdateEvent(WeekViewEvent event) {
        Intent editIntent = new Intent(activity, EditEventActivity.class);
        editIntent.putExtra("event", event.getId());
        activity.startActivityForResult(editIntent, ADD_EVENT);
    }

    public void setEvents(ArrayList<WeekViewEvent> events) {
        this.allEvents = events;
    }

    public void showEvents(Date selectedDate) {
        lastPosition = -1;
        events.clear();
        for (int i = 0; i < allEvents.size(); i++) {
            WeekViewEvent e = allEvents.get(i);
            Calendar cStart = Calendar.getInstance();
            cStart.setTimeInMillis(e.getStartTime());

            if (df.format(cStart.getTime()).equals(df.format(selectedDate))){
                events.add(e);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(activity, android.R.anim.fade_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public class EventsViewHolder extends RecyclerView.ViewHolder {
        View viewEventColor;
        TextView tvTypeOfService, tvStartTime, tvEndTime;

        public EventsViewHolder(View view) {
            super(view);
            viewEventColor = view.findViewById(R.id.viewEventColor);
            tvTypeOfService = view.findViewById(R.id.tvTypeOfService);
            tvStartTime = view.findViewById(R.id.tvStartTime);
            tvEndTime = view.findViewById(R.id.tvEndTime);
        }
    }
}
